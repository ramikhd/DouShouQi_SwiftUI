//
//  MainView.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import SwiftUI

struct MainView: View {
    
    // MARK: - Properties

    @StateObject private var appState = AppState.shared

    // MARK: - Body

    var body: some View {
        Group {
            switch appState.currentState {
            case .inNaviguation:
                MenuView()
            case .inGame(let game, let isSpriteView):
                GameView(game: game, isSpriteView: isSpriteView)
            case .finishGame(let game, let result, let time):
                EndGameView(game: game, result: result, time: time)
            }
        }
        .preferredColorScheme(appState.appColorScheme.scheme)
        .environment(\.locale, .init(identifier: appState.appLanguage.rawValue))
        .environmentObject(appState)
    }
}

#Preview {
    MainView()
}
