//
//  Languages.swift
//  DouShouQI
//
//  Created by Rami Khedair on 17/05/2024.
//

import Foundation
import SwiftUI

enum Languages: String, AppEnumProtocol {
    var id: Self { self }

    case french = "fr"
    case english = "en"
    case arabic = "ar"

    
    var label: LocalizedStringKey {
        switch self {
        case .french:
            R.string.settingsFrench
        case .english:
            R.string.settingsEnglish
        case .arabic:
            R.string.settingsArabic
        }
    }
}
