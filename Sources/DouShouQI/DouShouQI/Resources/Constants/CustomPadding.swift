//
//  CustomPadding.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import Foundation

enum CustomPadding: CGFloat {
    case xs = 8
    case s = 16
    case m = 24
    case l = 32
    case xl = 40
    case xxl = 48
}
