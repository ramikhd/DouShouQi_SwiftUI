//
//  RImage.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import Foundation
import SwiftUI

struct RImage {
    let xmark = Image(systemName: "x.circle.fill")
    let check = Image(systemName: "checkmark.circle.fill")
    let settings = Image(systemName: "gearshape.fill")
    let play = Image(systemName: "play.circle.fill")
    let edit = Image(systemName: "square.and.pencil.circle.fill")
    let historic = Image(systemName: "book.pages.fill")
    let saves = Image(systemName: "square.and.arrow.down.fill")
    let player = Image(systemName: "person.crop.circle")
    let gt = Image(systemName: "greaterthan.square.fill")
    let camera = Image(systemName: "camera")
    let xmarkSimple = Image(systemName: "xmark")
    let questionmark = Image(systemName: "questionmark")
    
    let arrow = Image("arrow")
    
    let winner = Image("winner")
    let loser = Image("loser")
    
    let logo = Image("logo")
    let hemlet = Image("hemlet")

    let cat = Image("cat")
    let dog = Image("dog")
    let rat = Image("rat")
    let lion = Image("lion")
    let tiger = Image("tiger")
    let elephant = Image("elephant")
    let leopard = Image("leopard")
    let wolf = Image("wolf")
}

struct RUIImage {
    let arrow = UIImage(named: "arrow")
}
