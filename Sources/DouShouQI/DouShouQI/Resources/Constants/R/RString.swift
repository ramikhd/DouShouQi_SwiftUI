//
//  RString.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import Foundation
import SwiftUI

struct RString {
    
    let ok = LocalizedStringKey("ok")
    let confirm = LocalizedStringKey("confirm")
    
    // MARK: - MenuView
    
    let menuPlay = LocalizedStringKey("menu.play")
    let menuPlayArKit = LocalizedStringKey("menu.play.arkit")
    let menuHistoric = LocalizedStringKey("menu.historic")
    let menuSaves = LocalizedStringKey("menu.saves")
    let menuStart = LocalizedStringKey("menu.start")
    let menuNext = LocalizedStringKey("menu.next")
    
    // MARK: - SettingsView
    
    let settingsTitle = LocalizedStringKey("settings.title")
    let settingsColorScheme = LocalizedStringKey("settings.colorscheme")
    let settingsDark = LocalizedStringKey("settings.dark")
    let settingsLight = LocalizedStringKey("settings.light")
    let settingsLanguage = LocalizedStringKey("settings.language")
    let settingsArabic = LocalizedStringKey("settings.arabic")
    let settingsEnglish = LocalizedStringKey("settings.english")
    let settingsFrench = LocalizedStringKey("settings.french")
    let settingsPlayerOneColor = LocalizedStringKey("settings.player.color.one")
    let settingsPlayerTwoColor = LocalizedStringKey("settings.player.color.two")
    
    // MARK: - Game Setting
    
    let gameSettingsTitle = LocalizedStringKey("game.settings.title")
    
    let gameSettingsPlayers = LocalizedStringKey("game.settings.players")
    let gameSettingsPlayersName = LocalizedStringKey("game.settings.players.name")
    let gameSettingsPlayersType = LocalizedStringKey("game.settings.players.type")
    let gameSettingsPlayersOne = LocalizedStringKey("game.settings.players.one")
    let gameSettingsPlayersTwo = LocalizedStringKey("game.settings.players.two")
    
    let gameSettingsRules = LocalizedStringKey("game.settings.rules")
    let gameSettingsRulesClassicTitle = LocalizedStringKey("game.settings.rules.classic.title")
    let gameSettingsRulesClassicLabel = LocalizedStringKey("game.settings.rules.classic.label")
    let gameSettingsRulesSimpleTitle = LocalizedStringKey("game.settings.rules.simple.title")
    let gameSettingsRulesSimpleLabel = LocalizedStringKey("game.settings.rules.simple.label")
    
    let gameErrorInvalidBoard = LocalizedStringKey("game.error.invalidBoard")
    let gameErrorInvalidGame = LocalizedStringKey("game.error.invalidGame")
    let gameErrorInvalidMove = LocalizedStringKey("game.error.invalidMove")
    let gameErrorInvalidPlayer = LocalizedStringKey("game.error.invalidPlayer")
    let gameErrorInvalidRules = LocalizedStringKey("game.error.invalidRules")
    let gameErrorNextPlayerError = LocalizedStringKey("game.error.nextPlayerError")
    let gameErrorUnknown = LocalizedStringKey("game.error.unknown")
    
    func gameErrorBadPlayerId(message: String) -> LocalizedStringKey {
        LocalizedStringKey("game.error.badPlayerId \(message)")
    }
    
    let gameQuitTitle = LocalizedStringKey("game.quit.title")
    let gameQuitYes = LocalizedStringKey("game.quit.yes")
    let gameQuitNo = LocalizedStringKey("game.quit.no")
    
    // MARK: - ResultView
    
    let resultWinneris = LocalizedStringKey("result.winneris")
    let resultEqual = LocalizedStringKey("result.equal")
    let resultNotFinish = LocalizedStringKey("result.notfinish")
    let winningDenReached = LocalizedStringKey("winning.denReached")
    let winningNoMorePieces = LocalizedStringKey("winning.noMorePieces")
    let winningTooManyOccurences = LocalizedStringKey("winning.tooManyOccurences")
    let winningNoMovesLeft = LocalizedStringKey("winning.noMovesLeft")
    
    // MARK: - Historic
    
    let historicTitle = LocalizedStringKey("historic.title")
    let historicVictory = LocalizedStringKey("historic.victory")
    let historicDefeat = LocalizedStringKey("historic.defeat")
    
    // MARK: - Saves
    
    let savesTitle = LocalizedStringKey("saves.title")
    
    // MARK: - Player
    
    let boardImage = "board"
    let boardSimpleImage = "board_simple"
    let hemlet = "hemlet"
    
    let catImageName = "cat"
    let dogImageName = "dog"
    let ratImageName = "rat"
    let lionImageName = "lion"
    let tigerImageName = "tiger"
    let elephantImageName = "elephant"
    let leopardImageName = "leopard"
    let wolfImageName = "wolf"
    
    // MARK: - Image Name
    
    let playerTypeHuman = LocalizedStringKey("player.type.human")
    let playerTypeIA = LocalizedStringKey("player.type.ia")
    let playerTypeRandom = LocalizedStringKey("player.type.random")
    
    // MARK: - EndGame
    
    let gameInfo = LocalizedStringKey("game.info")
    let gameTime = LocalizedStringKey("game.time")
    let playerNumberMove = LocalizedStringKey("player.number.move")
    let playerNumberPiece = LocalizedStringKey("player.number.piece")
    
    // MARK: - Take Picture
    
    let done = LocalizedStringKey("done")
    let cancel = LocalizedStringKey("cancel")
    let playerOnePicture = LocalizedStringKey("player.picture.one")
    let playerTwoPicture = LocalizedStringKey("player.picture.two")
}
