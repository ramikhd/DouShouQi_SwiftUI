//
//  RColor.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import Foundation
import SwiftUI

struct RColor {
    let dsqPrimary = Color("DSQPrimary")
    let onPrimary = Color("OnPrimary")
    let dsqSecondary = Color("DSQSecondary")
    let onSecondary = Color("OnSecondary")
    let dsqTertiary = Color("DSQTertiary")
    let dsqTertiaryVariant = Color("DSQTertiaryVariant")
    let onTertiary = Color("OnTertiary")
    let dsqError = Color("DSQError")
    let onError = Color("OnError")
    let background = Color("Background")
    let forground = Color("Forground")
    let dsqPlayerOne = Color("DSQPlayerOne")
    let dsqPlayerTwo = Color("DSQPlayerTwo")
}

struct RUIColor {
    let dsqSecondary = UIColor(named: "DSQSecondary")!
    let dsqTertiary = UIColor(named: "DSQTertiary")!
}
