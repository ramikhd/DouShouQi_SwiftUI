//
//  R.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import Foundation

struct R {
    
    // MARK: - SwiftUI

    static let string: RString = RString()
    static let color: RColor = RColor()
    static let image: RImage = RImage()
    static let font: RFont = RFont()
    
    // MARK: - UIKit

    static let uifont: RUIFont = RUIFont()
    static let uicolor: RUIColor = RUIColor()
    static let uiimage: RUIImage = RUIImage()
}
