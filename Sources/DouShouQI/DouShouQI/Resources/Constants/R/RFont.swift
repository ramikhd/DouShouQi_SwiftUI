//
//  RFont.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import SwiftUI

struct RFont {
    enum FontName: String {
        case ChineseAsianStyle = "Chinese Asian Style"
        case PoetsenOne = "PoetsenOne"
    }
    
    func ChineseAsianStyle(withSize size: CGFloat) -> Font {
        Font.custom(FontName.ChineseAsianStyle.rawValue, size: size)
    }
    
    func PoetsenOne(withSize size: CGFloat) -> Font {
        Font.custom(FontName.PoetsenOne.rawValue, size: size)
    }
}

struct RUIFont {
    
    func PoetsenOne(withSize size: CGFloat) -> UIFont {
        UIFont(name: RFont.FontName.PoetsenOne.rawValue, size: size)!
    }
}
