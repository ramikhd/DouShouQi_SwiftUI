//
//  TextValue.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import SwiftUI

enum TextValue {
    case h1, h2, h3
    case p1, p2, p3
    
    var font: Font {
        R.font.PoetsenOne(withSize: self.size)
    }
    
    var size: CGFloat {
        switch self {
        case .h1:
            28
        case .h2:
            24
        case .h3:
            20
        case .p1:
            16
        case .p2:
            14
        case .p3:
            12
        }
    }
    
    var weight: Font.Weight {
        switch self {
        case .h1, .h2, .h3:
            .semibold
        case .p1, .p2, .p3:
            .medium
        }
    }
}
