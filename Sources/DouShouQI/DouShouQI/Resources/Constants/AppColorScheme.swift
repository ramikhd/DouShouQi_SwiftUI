//
//  AppColorScheme.swift
//  DouShouQI
//
//  Created by Loris Perret on 15/05/2024.
//

import Foundation
import SwiftUI

protocol AppEnumProtocol: CaseIterable, Hashable {
    var label: LocalizedStringKey { get }
}

enum AppColorScheme: String, AppEnumProtocol {
    case light, dark
    
    var label: LocalizedStringKey {
        switch self {
        case .light:
            R.string.settingsLight
        case .dark:
            R.string.settingsDark
        }
    }
    
    var scheme: ColorScheme {
        switch self {
        case .light:
            .light
        case .dark:
            .dark
        }
    }
}
