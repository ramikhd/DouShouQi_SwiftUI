//
//  CustomPaddingModifier.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import Foundation
import SwiftUI

struct PaddingModifier: ViewModifier {

    // MARK: - Properties

    var edges: Edge.Set
    var length: CGFloat

    // MARK: - Body

    func body(content: Content) -> some View {
        content
            .padding(edges, length)
    }
}

extension View {
    func customPadding(_ edges: Edge.Set, _ length: CustomPadding) -> some View {
        modifier(PaddingModifier(edges: edges, length: length.rawValue))
    }
    func customPadding(_ length: CustomPadding) -> some View {
        modifier(PaddingModifier(edges: .all, length: length.rawValue))
    }
    
    func customPadding(_ edges: Edge.Set = .all, _ length: CGFloat) -> some View {
        modifier(PaddingModifier(edges: edges, length: length))
    }
    func customPadding(_ length: CGFloat) -> some View {
        modifier(PaddingModifier(edges: .all, length: length))
    }
}
