//
//  TextModifier.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import SwiftUI

struct TextModifier: ViewModifier {
    
    // MARK: - Properties

    let style: TextValue
    
    // MARK: - Body

    func body(content: Content) -> some View {
        content
            .font(style.font)
            .fontWeight(style.weight)
    }
}

extension View {
    func text(_ style: TextValue) -> some View {
        self.modifier(TextModifier(style: style))
    }
}
