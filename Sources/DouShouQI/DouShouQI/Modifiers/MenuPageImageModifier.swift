//
//  MenuPageImageModifier.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import Foundation
import SwiftUI

extension Image {
    func menuPageImage(rotation: CGFloat, size: CGFloat = 150) -> some View {
        self
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: size, height: size)
            .rotationEffect(.degrees(rotation))
    }
}
