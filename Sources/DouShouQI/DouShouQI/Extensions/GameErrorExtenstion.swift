//
//  GameErrorExtenstion.swift
//  DouShouQI
//
//  Created by Loris Perret on 29/05/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

extension GameError {
    
    var label: LocalizedStringKey {
        switch self {
        case .invalidBoard:
            R.string.gameErrorInvalidBoard
        case .invalidGame:
            R.string.gameErrorInvalidGame
        case .invalidMove:
            R.string.gameErrorInvalidMove
        case .invalidPlayer:
            R.string.gameErrorInvalidPlayer
        case .invalidRules:
            R.string.gameErrorInvalidRules
        case .nextPlayerError:
            R.string.gameErrorNextPlayerError
        case .badPlayerId(let message):
            R.string.gameErrorBadPlayerId(message: message)
        @unknown default:
            R.string.gameErrorUnknown
        }
    }
}
