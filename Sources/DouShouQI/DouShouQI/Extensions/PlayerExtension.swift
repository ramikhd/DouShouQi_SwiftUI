//
//  PlayerExtension.swift
//  DouShouQI
//
//  Created by Loris Perret on 24/05/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

extension Player {
    enum PlayerType: AppEnumProtocol {
        
        case human
        case random
//        case IA
        
        var label: LocalizedStringKey {
            switch self {
            case .human:
                R.string.playerTypeHuman
            case .random:
                R.string.playerTypeRandom
//            case .IA:
//                R.string.playerTypeIA
            }
        }
        
        func instance(withName name: String, andId id: Owner) -> Player? {
            switch self {
            case .human:
                HumanPlayer(withName: name, andId: id)
            case .random:
                RandomPlayer(withName: name, andId: id)
//            case .IA:
//                IAPlayer(withName: name, andId: id)
            }
        }
    }
    
    var playerType: PlayerType {
//        if self is IAPlayer {
//            return .IA
//        }
        
        if self is HumanPlayer {
            return .human
        }
        
        return .random
    }
}
