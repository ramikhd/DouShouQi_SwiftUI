//
//  RulesExtension.swift
//  DouShouQI
//
//  Created by Loris Perret on 29/05/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

enum RulesType: AppEnumProtocol {
    
    case classic
    case simple
    
    var label: LocalizedStringKey {
        switch self {
        case .classic:
            R.string.gameSettingsRulesClassicLabel
        case .simple:
            R.string.gameSettingsRulesSimpleLabel
        }
    }
    
    var title: LocalizedStringKey {
        switch self {
        case .classic:
            R.string.gameSettingsRulesClassicTitle
        case .simple:
            R.string.gameSettingsRulesSimpleTitle
        }
    }
    
    var instance: Rules {
        switch self {
        case .classic:
            ClassicRules()
        case .simple:
            VerySimpleRules()
        }
    }
    
    func instance(occurences: [Board : Int], historic: [Move]) -> Rules {
        switch self {
        case .classic:
            ClassicRules(occurences: occurences, historic: historic)
        case .simple:
            VerySimpleRules(occurences: occurences, historic: historic)
        }
    }
}

extension Rules {
    var type: RulesType {
        if self is VerySimpleRules {
            return .simple
        }
        
        return .classic
    }
    
    var boardImageName: String {
        if self is VerySimpleRules {
            return R.string.boardSimpleImage
        }
        
        return R.string.boardImage
    }
    
    var boardSize: CGSize {
        if self is VerySimpleRules {
            return CGSize(width: 540, height: 540)
        }
        
        return CGSize(width: 740, height: 940)
    }
    
    var offset: CGPoint {
        if self is VerySimpleRules {
            return CGPoint(x: -200, y: -200)
        }
        
        return CGPoint(x: -300, y: -400)
    }

}
