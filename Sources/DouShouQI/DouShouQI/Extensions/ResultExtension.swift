//
//  ResultExtension.swift
//  DouShouQI
//
//  Created by Loris Perret on 06/06/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

extension DouShouQiModel.Result {
    public var description: LocalizedStringKey {
        switch self {
        case .even:
            R.string.resultEqual
        case .notFinished:
            R.string.resultEqual
        case .winner(let winner, _):
            R.string.resultWinneris
        }
    }
}
