//
//  Player+Mock.swift
//  DouShouQI
//
//  Created by Loris Perret on 22/05/2024.
//

import Foundation
import DouShouQiModel

extension Player {
    enum Mock {
        static let player1 : Player = HumanPlayer(withName: "Loris", andId: .player1)!
        static let player2 : Player = HumanPlayer(withName: "Rami", andId: .player2)!
    }
}
