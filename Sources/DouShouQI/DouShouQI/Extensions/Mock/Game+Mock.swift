//
//  Game+Mock.swift
//  DouShouQI
//
//  Created by Loris Perret on 22/05/2024.
//

import Foundation
import DouShouQiModel

extension Game {
    enum Mock {
        static let previewGame = try! Game(withRules: ClassicRules(), andPlayer1: Player.Mock.player1, andPlayer2: Player.Mock.player2)
        static let previewGame2 = try! Game(withRules: ClassicRules(), andPlayer1: Player.Mock.player2, andPlayer2: Player.Mock.player2)
    }
}
