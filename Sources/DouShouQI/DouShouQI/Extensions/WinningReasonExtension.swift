//
//  WinningReasonExtension.swift
//  DouShouQI
//
//  Created by Loris Perret on 06/06/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

extension WinningReason {
    public var description: LocalizedStringKey {
        switch self {
        case .denReached:
            LocalizedStringKey("winning.denReached")
//            "the opponent's den has been reached."
        case .noMorePieces:
            LocalizedStringKey("winning.noMorePieces")
//            "the oponent has no more pieces."
        case .tooManyOccurences:
            LocalizedStringKey("winning.tooManyOccurences")
//            "too many occurences"
        case .noMovesLeft:
            LocalizedStringKey("winning.noMovesLeft")
//            "the oponent has no move left."
        }
    }
}
