//
//  Owner.swift
//  DouShouQI
//
//  Created by Rami Khedair on 27/05/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

extension Owner {
    var color : Color {
        switch self{
        case .noOne:
            return Color(.clear)
        case .player1:
            return AppState.shared.playerOneColor
        case .player2:
            return AppState.shared.playerTwoColor
        }
    }
    
    var animationWinFileName : String {
        switch self{
        case .noOne:
            return "tie.json"
        case .player1:
            return "win.json"
        case .player2:
            return "win.json"
        }
    }
    
    static prefix func !(owner: Owner) -> Owner {
           switch(owner){
           case .noOne:
               return .noOne
           case .player1:
               return  .player2
           case .player2:
               return .player1
           }
       }
    
    
}
