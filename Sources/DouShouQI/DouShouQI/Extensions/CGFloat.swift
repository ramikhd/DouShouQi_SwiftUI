//
//  CGFloat.swift
//  DouShouQI
//
//  Created by Rami Khedair on 27/05/2024.
//

import Foundation

extension CGFloat{
    func getLeadingDigits(count: Int) -> CGFloat {
        let factor = pow(10.0, CGFloat(count))
        let leadingDigits = Int((self / factor).rounded())
                
        return CGFloat(leadingDigits)
    }
}
