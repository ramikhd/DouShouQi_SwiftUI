////
////  SimpleIAPlayer.swift
////
////
////  Created by Loris Perret on 01/02/2024.
////
//
//import Foundation
//import DouShouQiModel
//
///// Représente une IA simple qui quand elle peut manger une pièce elle va la manger
//
//public class SimpleIAPlayer: IAPlayer {
//
//    // MARK: - Initializer
//    
//    /// Créer un joueur
//    ///
//    /// - Parameters:
//    ///   - withName name: Le nom du joueur.
//    ///   - andId id: L'id du joueur.
//    public override init?(withName name: String, andId id: Owner) {
//        super.init(withName: name, andId: id)
//    }
//    
//    // MARK: - Methods
//    
//    /// Choisi un mouvement
//    ///
//    /// - Parameters:
//    ///   - in board: Le plateau.
//    ///   - with rules: Les règles du jeu.
//    public override func chooseMove(in board: Board, with rules: Rules) -> Move? {
//        let moves = rules.getMoves(on: board, for: id)
//        
//        let move = moves.first { move in
//            let start: Cell = board.grid[move.rowOrigin][move.columnOrigin]
//            let destination: Cell = board.grid[move.rowDestination][move.columnDestination]
//            
//            guard let startPiece = start.piece, let destinationPiece = destination.piece else { return false }
//            
//            return startPiece.canEat(piece: destinationPiece)
//        }
//        
//        guard let move else {
//            return moves.randomElement()
//        }
//            
//        return move
//    }
//}
