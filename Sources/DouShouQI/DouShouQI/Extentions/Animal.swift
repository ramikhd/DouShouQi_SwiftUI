//
//  Animal.swift
//  DouShouQI
//
//  Created by Rami Khedair on 21/05/2024.
//

import SwiftUI
import DouShouQiModel

extension Animal {
    public static var allCases: [Animal] = [
        .rat,
        .cat,
        .dog,
        .wolf,
        .leopard,
        .tiger,
        .lion,
        .elephant,
    ]
    
    var image: Image {
        switch self {
        case .rat:
            return R.image.rat
        case .cat:
            return R.image.cat
        case .dog:
            return R.image.dog
        case .wolf:
            return R.image.wolf
        case .leopard:
            return R.image.leopard
        case .tiger:
            return R.image.tiger
        case .lion:
            return R.image.lion
        case .elephant:
            return R.image.elephant
        }
    }
    
    var imageName: String {
        switch self {
        case .rat:
            return R.string.ratImageName
        case .cat:
            return R.string.catImageName
        case .dog:
            return R.string.dogImageName
        case .wolf:
            return R.string.wolfImageName
        case .leopard:
            return R.string.leopardImageName
        case .tiger:
            return R.string.tigerImageName
        case .lion:
            return R.string.lionImageName
        case .elephant:
            return R.string.elephantImageName
        }
    }
    
    var canEat: [Animal] {
        switch self {
        case .rat:
            [.rat, .elephant]
        case .cat:
            [.cat, .rat]
        case .dog:
            [.dog, .cat, .rat]
        case .wolf:
            [.wolf, .dog, .cat, .rat]
        case .leopard:
            [.leopard, .wolf, .dog, .cat, .rat]
        case .tiger:
            [.tiger, .leopard, .wolf, .dog, .cat, .rat]
        case .lion:
            [.lion, .tiger, .leopard, .wolf, .dog, .cat, .rat]
        case .elephant:
            [.elephant, .tiger, .leopard, .wolf, .dog, .cat]
        }
    }
}
