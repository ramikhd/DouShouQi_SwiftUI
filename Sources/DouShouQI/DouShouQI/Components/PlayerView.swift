//
//  PlayerView.swift
//  DouShouQI
//
//  Created by Rami Khedair on 19/05/2024.
//

import SwiftUI

struct PlayerView: View {
    
    // MARK: - Properties

    var forgroundColor: Color
    var name: String
    var image: UIImage?

    // MARK: - Body

    var body: some View {
        VStack {
            if let image {
                Image(uiImage: image)
                    .resizable()
                    .frame(width: 100, height: 100)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 2))
            } else {
                R.image.player
                    .resizable()
                    .frame(width: 100, height: 100)
                    .aspectRatio(contentMode: .fill)
            }
            
            Text(name)
                .text(.h1)
        }
        .foregroundColor(forgroundColor)
    }
}

#Preview {
    PlayerView(forgroundColor: R.color.dsqPrimary, name: "Player")
}
