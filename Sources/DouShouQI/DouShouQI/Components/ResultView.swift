//
//  ResultView.swift
//  DouShouQI
//
//  Created by Rami Khedair on 19/05/2024.
//

import SwiftUI
import DouShouQiModel

struct ResultView: View {
    
    // MARK: - Properties
    
    @EnvironmentObject private var appState: AppState
    var resultItem: ResultItem
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            PlayerView(
                forgroundColor: R.color.background,
                name: resultItem.playerOne.name,
                image: appState.playerOnePicture
            )
            
            score
            
            PlayerView(
                forgroundColor: R.color.background,
                name: resultItem.playerTwo.name,
                image: appState.playerTwoPicture
            )
        }
        .customPadding(.s)
        .background(background)
        .cornerRadius(CustomPadding.xxl.rawValue)
        .customPadding(.vertical, .s)
        .background(R.color.background)
    }
        
    // MARK: - Background
    
    @ViewBuilder
    private var background: some View {
        switch resultItem.result {
        case .even: equalBg
        case .notFinished: notFinishBg
        case .winner(let winner, _):
            if winner == resultItem.playerOne.id {
                playerOneBg
            } else {
                playerTwoBg
            }
        }
    }
    
    private var playerOneBg: some View {
        LinearGradient(
            gradient: Gradient(colors: [appState.playerOneColor]),
            startPoint: .leading,
            endPoint: .trailing
        )
    }
    
    private var playerTwoBg: some View {
        LinearGradient(
            gradient: Gradient(colors: [appState.playerTwoColor]),
            startPoint: .leading,
            endPoint: .trailing
        )
    }
    
    private var equalBg: some View {
        LinearGradient(
            gradient: Gradient(colors: [appState.playerOneColor, appState.playerTwoColor]),
            startPoint: .leading,
            endPoint: .trailing
        )
    }
    
    private var notFinishBg: some View {
        LinearGradient(
            gradient: Gradient(colors: [.gray]),
            startPoint: .leading,
            endPoint: .trailing
        )
    }
    
    // MARK: - Subviews
    
    private func finish(
        name: String,
        reason: LocalizedStringKey
    ) -> some View {
        VStack(spacing: CustomPadding.s.rawValue) {
            HStack {
                Text(R.string.resultWinneris)
                Text(name)
            }
            .text(.h1)
            
            Text(reason)
                .text(.h3)
        }
    }
    
    private var draw: some View {
        Text(R.string.resultEqual)
            .text(.h1)
    }
    
    private var notFinish: some View {
        Text(R.string.resultNotFinish)
            .text(.h1)
    }

    private var score: some View {
        HStack {
            switch resultItem.result {
            case .even: draw
            case .notFinished: notFinish
            case .winner(let winner, let reason):
                if winner == resultItem.playerOne.id {
                    finish(
                        name: resultItem.playerOne.name,
                        reason: reason.description
                    )
                } else {
                    finish(
                        name: resultItem.playerTwo.name,
                        reason: reason.description
                    )
                }
            }
        }
        .foregroundColor(R.color.background)
        .frame(maxWidth: .infinity)
    }
}

#Preview {
    ResultView(
        resultItem: ResultItem(
            playerOne: Player.Mock.player1,
            playerTwo: Player.Mock.player2,
            result: .winner(winner: .player1, reason: .denReached)
        )
    )
}

#Preview {
    ResultView(
        resultItem: ResultItem(
            playerOne: Player.Mock.player1,
            playerTwo: Player.Mock.player2,
            result: .winner(winner: .player2, reason: .denReached)
        )
    )
}
#Preview {
    ResultView(
        resultItem: ResultItem(
            playerOne: Player.Mock.player1,
            playerTwo: Player.Mock.player2,
            result: .even
        )
    )
}
#Preview {
    ResultView(
        resultItem: ResultItem(
            playerOne: Player.Mock.player1,
            playerTwo: Player.Mock.player2,
            result: .notFinished
        )
    )
}
