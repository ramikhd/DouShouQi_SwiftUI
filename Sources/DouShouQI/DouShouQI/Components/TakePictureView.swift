//
//  TakePictureView.swift
//  DouShouQI
//
//  Created by Loris Perret on 12/06/2024.
//

import SwiftUI

struct TakePictureView: View {

    // MARK: - Properties
    
    @EnvironmentObject var viewModel: CameraViewModel
    
    @State private var imageSize = CGFloat(100)
    @State private var isImageBig = false
    
    @Binding var image: UIImage?
    var nextState: CameraViewModel.StatePicture
    
    private let littleImageSize = CGFloat(100)
    private let bigImageSize = CGFloat(300)
    
    // MARK: - Body

    var body: some View {
        VStack {
            CameraViewControllerRepresentable(viewModel: viewModel)
                .edgesIgnoringSafeArea(.all)
                
            if let imageSave = viewModel.savedFace {
                VStack {
                    imageRender(image: imageSave)
                    validButton(image: imageSave)
                }
            } else {
                takePictureButton
            }
        }
    }
    
    // MARK: - Subviews

    private var takePictureButton: some View {
        Button {
            viewModel.startCapture()
        } label: {
            R.image.camera
                .renderingMode(.template)
                .foregroundStyle(R.color.onTertiary)
                .fontWeight(.bold)
                .customPadding(.s)
        }
        .background(R.color.dsqTertiary)
        .clipShape(Circle())
    }
    
    private func validButton(image imageSave: UIImage) -> some View {
        Button {
            image = imageSave
            viewModel.savedFace = nil
            viewModel.state = nextState
        } label: {
            IconButtonView(
                icon: R.image.check,
                text: R.string.menuNext
            )
        }
    }
    
    private var clearButton: some View {
        Button {
            viewModel.clearSavedFaces()
        } label: {
            R.image.xmarkSimple
                .renderingMode(.template)
                .foregroundStyle(R.color.onTertiary)
                .fontWeight(.bold)
                .customPadding(.xs)
        }
        .background(R.color.dsqTertiary)
        .clipShape(Circle())
    }
    
    private func imageRender(image: UIImage) -> some View {
        ZStack(alignment: .topTrailing) {
            Image(uiImage: image)
                .resizable()
                .frame(width: imageSize, height: imageSize)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 2))
                .customPadding(.xs)
                .onTapGesture {
                    imageSize = isImageBig ? littleImageSize : bigImageSize
                    isImageBig.toggle()
                }
            
            clearButton
        }
    }
}

#Preview {
    TakePictureView(image: .constant(R.uiimage.arrow), nextState: .player1)
}
