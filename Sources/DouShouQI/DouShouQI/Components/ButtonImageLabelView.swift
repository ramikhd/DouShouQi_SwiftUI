//
//  ButtonImageLabelView.swift
//  DouShouQI
//
//  Created by Loris Perret on 25/05/2024.
//

import SwiftUI

struct ButtonImageLabelView: View {
    
    // MARK: - Properties

    let image: Image
    let color: Color
    let action: () -> Void
    
    // MARK: - Initializer

    init(
        image: Image,
        color: Color = R.color.dsqTertiary,
        action: @escaping () -> Void
    ) {
        self.image = image
        self.color = color
        self.action = action
    }
    
    // MARK: - Body
    
    var body: some View {
        Button {
            action()
        } label: {
            image
                .resizable()
                .renderingMode(.template)
                .frame(width: 48, height: 48)
                .foregroundStyle(color)
        }
    }
}

#Preview {
    ButtonImageLabelView(image: R.image.play) {}
}
