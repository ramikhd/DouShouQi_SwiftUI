//
//  GameMenuEditingSheetView.swift
//  DouShouQI
//
//  Created by Loris Perret on 23/05/2024.
//

import SwiftUI
import DouShouQiModel

struct GameMenuEditingSheetView: View {
    
    // MARK: - Properties

    @Binding var initialGame: GameMenuItem
    @Binding var isPresented: Bool
    
    @State private var newGame: GameMenuItem
    
    // MARK: - Initializer
    
    init(initialGame: Binding<GameMenuItem>, isPresented: Binding<Bool>) {
        self._initialGame = initialGame
        self._isPresented = isPresented
        self.newGame = GameMenuItem(
            playerOneName: initialGame.wrappedValue.playerOneName,
            playerTwoName: initialGame.wrappedValue.playerTwoName,
            playerOneType: initialGame.wrappedValue.playerOneType,
            playerTwoType: initialGame.wrappedValue.playerTwoType,
            rulesType: initialGame.wrappedValue.rulesType
        )
    }
    
    // MARK: - Body

    var body: some View {
        ScrollView {
            VStack {
                buttons
                
                rules(type: $newGame.rulesType)
                
                player(name: $newGame.playerOneName, type: $newGame.playerOneType)
                
                player(name: $newGame.playerTwoName, type: $newGame.playerTwoType)
                
                Button {
                    initialGame = newGame
                    isPresented.toggle()
                } label: {
                    IconButtonView(
                        icon: R.image.check,
                        text: R.string.confirm,
                        backgroundColor: R.color.dsqPrimary,
                        foregroundColor: R.color.onPrimary
                    )
                }
            }
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: .top
        )
        .customPadding(.xl)
        .background(R.color.background)
    }
    
    // MARK: - Subviews

    private var buttons: some View {
        HStack {
            ButtonImageLabelView(image: R.image.xmark) {
                isPresented.toggle()
            }
            
            ButtonImageLabelView(image: R.image.check) {
                initialGame = newGame
                isPresented.toggle()
            }
        }
        .frame(maxWidth: .infinity, alignment: .trailing)
    }
    
    private func rules(type: Binding<RulesType>) -> some View {
        CustomPickerView(
            R.string.gameSettingsRules,
            selection: type,
            items: RulesType.allCases
        )
        .customPadding(.m)
    }
    
    private func player(
        name: Binding<String>,
        type: Binding<Player.PlayerType>
    ) -> some View {
        VStack {
            nameView(name)
            typeView(type)
        }
        .customPadding(.m)
    }
    
    private func nameView(_ name: Binding<String>) -> some View {
        VStack(alignment: .leading) {
            Text(R.string.gameSettingsPlayersName)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
            
            HStack(spacing: CustomPadding.m.rawValue) {
                R.image.gt
                    .resizable()
                    .renderingMode(.template)
                    .foregroundStyle(R.color.onTertiary)
                    .frame(width: 24, height: 24)
                
                TextField(
                    R.string.gameSettingsPlayersName,
                    text: name
                )
                .textFieldStyle(.roundedBorder)
                .text(.h3)
                .foregroundStyle(R.color.dsqTertiary)
                .frame(maxWidth: .infinity)
            }
            .customPadding(.s)
            .customPadding(.horizontal, .xs)
            .background {
                RoundedRectangle(cornerRadius: 16)
                    .fill(R.color.dsqTertiary)
            }
        }
    }
    
    private func typeView(_ type: Binding<Player.PlayerType>) -> some View {
        CustomPickerView(
            R.string.gameSettingsPlayersType,
            selection: type,
            items: Player.PlayerType.allCases
        )
    }
}

#Preview {
    GameMenuEditingSheetView(initialGame: .constant(GameMenuItem()), isPresented: .constant(false))
}
