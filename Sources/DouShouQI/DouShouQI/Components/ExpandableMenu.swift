//
//  ExpandableMenu.swift
//  DouShouQI
//
//  Created by Loris Perret on 24/05/2024.
//

import SwiftUI

struct ExpandableMenu<Content: View>: View {

    // MARK: - Properties

    let title: LocalizedStringKey
    let content: Content
    let primaryColor: Color
    let secondaryColor: Color
    let textColor: Color
    
    @Binding private var isOpen: Bool

    // MARK: - Initializer
    
    init(
        title: LocalizedStringKey,
        isOpen: Binding<Bool>,
        primaryColor: Color = R.color.dsqTertiary,
        secondaryColor: Color = R.color.dsqTertiaryVariant,
        textColor: Color = R.color.onTertiary,
        @ViewBuilder content: () -> Content
    ) {
        self.title = title
        self._isOpen = isOpen
        self.content = content()
        self.primaryColor = primaryColor
        self.secondaryColor = secondaryColor
        self.textColor = textColor
    }

    // MARK: - Body

    var body: some View {
        VStack {
            Text(title)
                .text(.h1)
                .customPadding(.l)
                .frame(maxWidth: .infinity)
                .foregroundStyle(textColor)
                .background(primaryColor)
                .clipShape(RoundedRectangle(cornerRadius: 25.0))
            
            if isOpen {
                content
            }
        }
        .frame(maxWidth: .infinity)
        .background(secondaryColor)
        .clipShape(RoundedRectangle(cornerRadius: 25.0))
        .onTapGesture {
            withAnimation {
                isOpen.toggle()
            }
        }
    }
}

#Preview {
    ExpandableMenu(title: R.string.menuPlay, isOpen: .constant(false)) {
        Text(R.string.menuPlay)
            .text(.h1)
            .customPadding(.l)
            .frame(maxWidth: .infinity)
            .foregroundStyle(R.color.onTertiary)
    }
}
