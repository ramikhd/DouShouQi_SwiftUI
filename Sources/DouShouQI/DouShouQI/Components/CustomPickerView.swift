//
//  CustomPickerView.swift
//  DouShouQI
//
//  Created by Loris Perret on 26/05/2024.
//

import SwiftUI

struct CustomPickerView<Item: AppEnumProtocol>: View {
    
    // MARK: - Properties
    
    @Binding private var selection: Item
    private let title: LocalizedStringKey
    private let items: [Item]
    private let backgroundColor: Color
    private let forgroundColor: Color
    
    // MARK: - Initializer

    public init(
        _ title: LocalizedStringKey,
        selection: Binding<Item>,
        items: [Item],
        backgroundColor: Color = R.color.dsqTertiary,
        forgroundColor: Color = R.color.onTertiary
    ) {
        self.title = title
        self._selection = selection
        self.items = items
        self.backgroundColor = backgroundColor
        self.forgroundColor = forgroundColor
    }
    
    // MARK: - Body

    var body: some View {
        VStack(alignment: .leading) {
            Text(title)
                .text(.h1)
                .foregroundStyle(backgroundColor)
            
            VStack(
                alignment: .leading,
                spacing: CustomPadding.s.rawValue
            ) {
                ForEach(items, id: \.self) { item in
                    itemView(item: item)
                }
            }
            .customPadding(.s)
            .frame(maxWidth: .infinity)
            .background {
                RoundedRectangle(cornerRadius: 16)
                    .fill(backgroundColor)
            }
        }
    }
    
    // MARK: - Subviews

    private func itemView(item: Item) -> some View {
        VStack(
            alignment: .leading,
            spacing: CustomPadding.s.rawValue
        ) {
            Button {
                selection = item
            } label: {
                HStack(spacing: CustomPadding.m.rawValue) {
                    Group {
                        if item == selection {
                            R.image.check
                                .resizable()
                                .renderingMode(.template)
                        } else {
                            Rectangle()
                                .fill(backgroundColor)
                        }
                    }
                    .frame(width: 24, height: 24)
                    .customPadding(.horizontal, .xs)
                    
                    Text(item.label)
                        .text(.h3)
                }
                .foregroundStyle(forgroundColor)
                .frame(maxWidth: .infinity, alignment: .leading)
            }
            
            if item != items.last {
                Divider()
                    .frame(height: 2)
                    .overlay(forgroundColor)
                
            }
        }
    }
}

#Preview {
    CustomPickerView(
        R.string.settingsColorScheme,
        selection: .constant(AppState.shared.appColorScheme),
        items: AppColorScheme.allCases
    )
}
