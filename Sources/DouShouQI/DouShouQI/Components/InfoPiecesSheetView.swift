//
//  InfoPiecesSheetView.swift
//  DouShouQI
//
//  Created by Loris Perret on 17/06/2024.
//

import SwiftUI
import DouShouQiModel

struct InfoPiecesSheetView: View {
    
    // MARK: - Properties

    @State var animalList: [Animal]
    
    // MARK: - Body

    var body: some View {
        ScrollView {
            VStack {
                ForEach(animalList, id: \.rawValue) { animal in
                    HStack(spacing: 10) {
                        animal.image
                            .resizable()
                            .frame(width: 100, height: 100)
                        
                        power(value: animal.rawValue)
                        
                        Text(":")
                            .text(.h1)
                            .foregroundStyle(R.color.dsqTertiary)
                            .customPadding(.trailing, .s)
                        
                        canEat(list: animal.canEat.filter({ a in
                            animalList.contains(a)
                        }))
                    }
                }
            }
            .customPadding(.l)
        }
        .background(R.color.background)
    }
    
    // MARK: - Subviews
    
    private func power(value: Int) -> some View {
        Text("(\(value))")
            .text(.h1)
            .foregroundStyle(R.color.dsqTertiary)
    }

    private func canEat(list: [Animal]) -> some View {
        ScrollView(.horizontal) {
            HStack {
                ForEach(list, id: \.rawValue) { other in
                    other.image
                        .resizable()
                        .frame(width: 100, height: 100)
                }
            }
        }
    }
}

#Preview {
    InfoPiecesSheetView(animalList: [
        .cat,
        .leopard
    ])
}
