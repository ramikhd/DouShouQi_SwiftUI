//
//  IconButtonView.swift
//  DouShouQI
//
//  Created by Loris Perret on 14/05/2024.
//

import SwiftUI

struct IconButtonView: View {
    
    // MARK: - Properties
    
    var icon: Image
    var text: LocalizedStringKey
    var backgroundColor: Color = R.color.dsqTertiary
    var foregroundColor: Color = R.color.onTertiary
    var width: CGFloat = 250
    
    // MARK: - Body
    
    var body: some View {
        HStack(spacing: CustomPadding.s.rawValue) {
            icon
                .resizable()
                .renderingMode(.template)
                .frame(width: 32, height: 32)
                .customPadding(.leading, .s)
            
            Text(text)
                .text(.h1)
                .frame(maxWidth: .infinity, alignment: .center)
        }
        .foregroundStyle(foregroundColor)
        .frame(maxWidth: width, alignment: .leading)
        .customPadding(.s)
        .background(backgroundColor)
        .clipShape(RoundedRectangle(cornerRadius: 16))
    }
}

#Preview {
    IconButtonView(
        icon: R.image.play,
        text: R.string.menuPlay,
        backgroundColor: R.color.dsqPrimary,
        foregroundColor: R.color.onPrimary
    )
}
