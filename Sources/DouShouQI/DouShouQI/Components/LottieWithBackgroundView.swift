//
//  LottieWithBackgroundView.swift
//  DouShouQI
//
//  Created by Loris Perret on 01/06/2024.
//

import SwiftUI
import Lottie

struct LottieWithBackgroundView: View {
    
    // MARK: - Properties

    var color: Color = .black
    var animation: String
    var action: () -> Void

    // MARK: - Body

    var body: some View {
        ZStack {
            color.opacity(0.8)
                .ignoresSafeArea()
            
            LottieView(animation: LottieAnimation.named(animation))
                .playing(loopMode: .playOnce)
                .animationDidFinish({ completed in
                    action()
                })
                .resizable()
                .customPadding(.xxl)
        }
    }
}

#Preview {
    LottieWithBackgroundView(animation: "tie.json", action: {})
}
