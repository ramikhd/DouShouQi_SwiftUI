//
//  DouShouQIApp.swift
//  DouShouQI
//
//  Created by Loris Perret on 13/05/2024.
//

import SwiftUI

@main
struct DouShouQIApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
