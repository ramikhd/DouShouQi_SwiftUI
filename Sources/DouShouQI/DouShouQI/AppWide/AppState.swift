//
//  AppState.swift
//  DouShouQI
//
//  Created by Loris Perret on 15/05/2024.
//

import Foundation
import SwiftUI
import DouShouQiModel

class AppState: ObservableObject {
    static let shared = AppState()
    
    // MARK: - Properties
    
    @Published var appLanguage: Languages = Languages.french {
        didSet {
            AppUserDefault.language = appLanguage
        }
    }
    
    @Published var appColorScheme: AppColorScheme {
        didSet {
            AppUserDefault.colorScheme = appColorScheme
        }
    }
    
    @Published var currentState: AppStateEnum = .inNaviguation
    
    @Published var playerOneColor: Color = .dsqPlayerOne
    @Published var playerTwoColor: Color = .dsqPlayerTwo
    
    @Published var playerOnePicture: UIImage?
    @Published var playerTwoPicture: UIImage?
    
    let semaphoreTurn = DispatchSemaphore(value: 0)
    
    // MARK: - Private Init
    
    private init() {
        self.appColorScheme = AppUserDefault.colorScheme
        self.appLanguage = AppUserDefault.language
    }
}

enum AppStateEnum {
    case inNaviguation
    case inGame(game: Game, isSpriteView: Bool)
    case finishGame(game: Game, result: DouShouQiModel.Result, time: Int)
}
