//
//  AppDelegate.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import Foundation
import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        setUpNaviguationBar()
        
        return true
    }
    
    // MARK: - Naviguation bar

    private func setUpNaviguationBar() {
        // Back image
        let backImage = R.uiimage.arrow
        
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        
        // Back title
        let backButtonTitleAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.clear
        ] as [NSAttributedString.Key: Any]
        
        UIBarButtonItem.appearance().setTitleTextAttributes(backButtonTitleAttributes, for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes(backButtonTitleAttributes, for: .highlighted)
        
        // Title
        let titleAttributes = [
            NSAttributedString.Key.foregroundColor: R.uicolor.dsqTertiary,
            NSAttributedString.Key.font: R.uifont.PoetsenOne(withSize: 32)
        ] as [NSAttributedString.Key: Any]
        UINavigationBar.appearance().titleTextAttributes = titleAttributes
        
        // Large Title
        let largeTitleAttributes = [
            NSAttributedString.Key.foregroundColor: R.uicolor.dsqTertiary,
            NSAttributedString.Key.font: R.uifont.PoetsenOne(withSize: 40)
        ] as [NSAttributedString.Key: Any]
        UINavigationBar.appearance().largeTitleTextAttributes = largeTitleAttributes
        
        // Padding Title
        UINavigationBar.appearance().layoutMargins.left = CustomPadding.xxl.rawValue
    }
}
