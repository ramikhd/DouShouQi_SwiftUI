//
//  AppUserDefault.swift
//  DouShouQI
//
//  Created by Loris Perret on 16/05/2024.
//

import Foundation

struct AppUserDefault {
    
    // MARK: - Color

    private static var colorSchemeKey = "ColorScheme"
    static var colorScheme: AppColorScheme {
        get {
            guard let userDefault = UserDefaults.standard.object(forKey: colorSchemeKey) as? String,
                  let value = AppColorScheme(rawValue: userDefault) else { return .light }
            
            return value
        }
        
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: colorSchemeKey)
        }
    }
    
    // MARK: - Language

    private static var languageKey = "Language"
    static var language: Languages {
        get {
            guard let userDefault = UserDefaults.standard.object(forKey: languageKey) as? String,
                  let value = Languages(rawValue: userDefault) else { return .french }
            
            return value
        }
        
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: languageKey)
        }
    }
}
