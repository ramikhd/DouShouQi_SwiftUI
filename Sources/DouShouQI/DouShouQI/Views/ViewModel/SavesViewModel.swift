//
//  SavesViewModel.swift
//  DouShouQI
//
//  Created by Loris Perret on 22/05/2024.
//

import Foundation
import DouShouQiModel

struct SavedGameItem: Identifiable {
    let id = UUID()
    
    let date: Date
    var formattedDate: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd / MM / yyyy - hh:mm"
        
        return formatter.string(from: date)
    }
    
    let game: Game
}

class SavesViewModel: ObservableObject {
    
    // MARK: - Methods

    func getSavedParties() -> [SavedGameItem] {
        [
            SavedGameItem(date: .now, game: Game.Mock.previewGame),
            SavedGameItem(date: .now - 100000, game: Game.Mock.previewGame),
            SavedGameItem(date: .now - 150000, game: Game.Mock.previewGame),
            SavedGameItem(date: .now - 50000, game: Game.Mock.previewGame),
        ].sorted { item1, item2 in
            item1.date > item2.date
        }
    }
}
