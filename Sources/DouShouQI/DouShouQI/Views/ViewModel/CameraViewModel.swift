//
//  CameraViewModel.swift
//  FaceTrackerSample
//
//  Created by Loris Perret on 12/06/2024.
//

import Foundation
import UIKit
import Vision

class CameraViewModel: ObservableObject {
    
    enum StatePicture {
        case player1, player2
        case finish
    }
    
    // MARK: - Properties

    @Published var isCapturingFaces = false
    @Published var savedFace: UIImage?
    @Published var state: StatePicture = .player1

    // MARK: - Methods

    func startCapture() {
        DispatchQueue.main.async {
            self.isCapturingFaces = true
        }
    }
    
    func clearSavedFaces() {
        DispatchQueue.main.async {
            self.savedFace = nil
        }
    }
    
    func saveImage(ciImg: CIImage, observation: VNFaceObservation) {
        let imgWidth = ciImg.extent.width
        let imgHeight = ciImg.extent.height
        let ciCtx = CIContext()
        
        let faceBox = observation.boundingBox
        let cropRect = CGRect(x: (1 - faceBox.minY - faceBox.height) * imgWidth,
                              y: (1 - faceBox.minX - faceBox.width) * imgHeight,
                              width: faceBox.height * imgWidth,
                              height: faceBox.width * imgHeight)
        
        if let cgImg = ciCtx.createCGImage(ciImg, from: cropRect) {
            let faceCrop = UIImage(cgImage: cgImg, scale: 1, orientation: .leftMirrored)
            
            DispatchQueue.main.async {
                self.savedFace = faceCrop
                self.isCapturingFaces = false
            }
        }
    }
}
