//
//  GameViewModel.swift
//  DouShouQI
//
//  Created by Loris Perret on 30/05/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI
import AVFoundation
import SpriteKit

@MainActor
class GameViewModel: ObservableObject {
    
    // MARK: - GameState

    enum State {
        case invalidMove
        case startGame
        case endGame(file: String, color: Color)
        case none
        
        var file: String {
            switch self {
            case .invalidMove:
                "error.json"
            case .startGame:
                "start.json"
            case .endGame(let file, _):
                file
            case .none:
                ""
            }
        }
    }
    
    // MARK: - Properties

    @Published var state: State = .none
    @Published var colors: [Color] = [AppState.shared.playerOneColor, AppState.shared.playerTwoColor]
    @Published var availableAnimalList: [Animal] = []
    @Published private var player: AVAudioPlayer?
    
    var currentPlayer: Owner = .player1
    
    var gameScene: GameScene = GameScene(withGame: Game.Mock.previewGame, size: CGSize(width: 0, height: 0))
    var player1: Player
    var player2: Player
    
    var gameResult: DouShouQiModel.Result = .notFinished
    var gameTime: Int = 0
    var startTime: Date = .now
    var game: Game = Game.Mock.previewGame
    
    private let soundNames = ["eat1", "eat2", "eat3", "eat4"]
    
    // MARK: - Initializer
    
    init(withGame game: Game) {
        self.player1 = game.players[.player1]!
        self.player2 = game.players[.player2]!
        
        availableAnimalList = getAnimalList(board: game.board)
    }
    
    // MARK: - Methods

    func playSound() {
        let selectedSound = soundNames.randomElement()
        
        guard let soundURL = Bundle.main.url(forResource: selectedSound, withExtension: "mp3") else {
            return
        }

        do {
            player = try AVAudioPlayer(contentsOf: soundURL)
            player?.play()
        } catch {
            print("Failed to load the sound: \(error)")
        }
    }
    
    public func getAnimalList(board: Board) -> [Animal] {
        var results: [Animal] = []
        
        for row in 0..<board.nbRows {
            for column in 0..<board.nbColumns {
                guard let piece = board.grid[row][column].piece else { continue }
                
                results.append(piece.animal)
            }
        }
        
        return Array(Set(results)).sorted { a1, a2 in
            a1.rawValue < a2.rawValue
        }
    }

    // MARK: - Listeners
    
    func invalidMoveListener(
        board: Board,
        move: Move,
        player: Player,
        result: Bool
    ) {
        print("Invalid move listener called")
        if result {
            return
        }
        
        DispatchQueue.main.sync {
            withAnimation {
                state = .invalidMove
            }
        }
    }
    
    func boardChanged(board: Board) {
        DispatchQueue.main.sync {
            withAnimation {
                availableAnimalList = getAnimalList(board: board)
            }
        }
    }
    
    func playerNotifiedListener(
        board: Board,
        player: Player
    ) async throws {
        print("Player notified listener called")
        
        currentPlayer = player.id
        
        withAnimation(.linear(duration: 1)) {
            if player.id == .player1 {
                colors = [
                    AppState.shared.playerOneColor,
                    AppState.shared.playerOneColor,
                    AppState.shared.playerOneColor,
                    AppState.shared.playerTwoColor
                ]
            } else {
                colors = [
                    AppState.shared.playerOneColor,
                    AppState.shared.playerTwoColor,
                    AppState.shared.playerTwoColor,
                    AppState.shared.playerTwoColor
                ]
            }
        }
    }
    
    func gameOverListener(
        board: Board,
        result: Result,
        player: Player?
    ) {
        print("Game over listener called")
        DispatchQueue.main.sync {
            gameResult = result
            gameTime = abs(Int(startTime.timeIntervalSinceNow.rounded()))
            
            if let playerNotNil = player {
                withAnimation {
                    state = .endGame(
                        file: playerNotNil.id.animationWinFileName,
                        color: playerNotNil.id.color
                    )
                }
            } else {
                withAnimation {
                    state = .endGame(file: Owner.noOne.animationWinFileName, color: .black)
                }
            }
        }
    }
    
    func gameStartedListener(board: Board) {
        print("Game started listener called")
        DispatchQueue.main.sync {
            withAnimation {
                state = .startGame
                startTime = .now
            }
        }
    }
    
    func pieceRemovedListener(
        row: Int,
        column: Int,
        piece: Piece
    ) {
        print("Piece removed listener called")
        DispatchQueue.main.sync {
            playSound()
        }
    }
}

class GameSpriteKitViewModel: GameViewModel {
    override init(withGame game: Game) {
        super.init(withGame: game)
        self.game = game

        self.game.addInvalidMoveCallbacksListener(invalidMoveListener)
        self.game.addPlayerNotifiedListener(playerNotifiedListener)
        self.game.addGameOverListener(gameOverListener)
        self.game.addGameStartedListener(gameStartedListener)
        self.game.addPieceRemovedListener(pieceRemovedListener)
        self.game.addBoardChangedListener(boardChanged)

        gameScene = GameScene(
            withGame: self.game,
            size: self.game.rules.boardSize
        )
    }
}


class GameARKitViewModel: GameViewModel {
    override init(withGame game: Game) {
        super.init(withGame: game)
        self.game = game

        self.game.addInvalidMoveCallbacksListener(invalidMoveListener)
        self.game.addPlayerNotifiedListener(playerNotifiedListener)
        self.game.addGameOverListener(gameOverListener)
        self.game.addGameStartedListener(gameStartedListener)
        self.game.addPieceRemovedListener(pieceRemovedListener)
        self.game.addBoardChangedListener(boardChanged)
    }
}
