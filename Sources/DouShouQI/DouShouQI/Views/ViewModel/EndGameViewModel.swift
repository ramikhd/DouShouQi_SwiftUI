//
//  EndGameViewModel.swift
//  DouShouQI
//
//  Created by Loris Perret on 24/06/2024.
//

import Foundation
import DouShouQiModel

class EndGameViewModel {
    
    // MARK: - Properties

    let game: Game
    let time: Int
    
    // MARK: - Initializer

    
    init(game: Game, time: Int) {
        self.game = game
        self.time = time
    }

    // MARK: - Methods

    func getMoves(forPlayer player: Player) -> Int {
        game.rules.historic.filter { move in
            move.owner == player.id
        }.count
    }
    
    func getPieces(forPlayer player: Player) -> Int {
        game.board.countPieces(of: player.id)
    }
    
    func getTime() -> String {
        var secondsString: String = ""
        var minutesString: String = ""
        var hoursString: String = ""
        
        guard time >= 60 else { return "\(time)s" }
        
        let (minutes, seconds) = time.quotientAndRemainder(dividingBy: 60)
        
        if seconds > 0 {
            secondsString = " \(seconds)s"
        }

        guard minutes >= 60 else { return "\(minutes)m" + secondsString }
        
        let (hours, minutes2) = minutes.quotientAndRemainder(dividingBy: 60)

        if minutes2 > 0 {
            minutesString = " \(minutes2)m"
        }
        
        guard hours > 24 else {
            return "\(hours)h" + minutesString + secondsString
        }
        
        let (days, hours2) = hours.quotientAndRemainder(dividingBy: 24)
        
        if hours2 > 0 {
            hoursString = " \(hours2)h"
        }
        
        return "\(days)j" + hoursString + minutesString + secondsString
    }
}
