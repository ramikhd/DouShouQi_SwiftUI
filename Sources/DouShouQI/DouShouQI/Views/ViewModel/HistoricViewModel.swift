//
//  HistoricViewModel.swift
//  DouShouQI
//
//  Created by Loris Perret on 22/05/2024.
//

import Foundation
import DouShouQiModel

struct ResultItem: Identifiable {
    
    let id: UUID = UUID()
    let playerOne: Player
    let playerTwo: Player
    let result: DouShouQiModel.Result
}

class HistoricViewModel: ObservableObject {
    
    // MARK: - Properties
    
    private var score: (Int, Int) = (10, 6)
    
    // MARK: - Methods

    func getHistoric() -> [ResultItem] {
        [
            ResultItem(
                playerOne: Player.Mock.player1,
                playerTwo: Player.Mock.player2,
                result: .winner(winner: .player1, reason: .denReached)
            ),
            ResultItem(
                playerOne: Player.Mock.player1,
                playerTwo: Player.Mock.player2,
                result: .winner(winner: .player2, reason: .noMorePieces)
            ),
            ResultItem(
                playerOne: Player.Mock.player1,
                playerTwo: Player.Mock.player2,
                result: .notFinished
            ),
            ResultItem(
                playerOne: Player.Mock.player1,
                playerTwo: Player.Mock.player2,
                result: .even
            ),
        ]
    }
    
    func getScore() -> (victories: Int, defeats: Int) {
        score
    }
}
