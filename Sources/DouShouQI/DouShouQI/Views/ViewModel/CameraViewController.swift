//
//  CameraView.swift
//  FaceTrackerSample
//
//  Created by Loris Perret on 12/06/2024.
//

import Foundation
import AVFoundation
import UIKit
import SwiftUI
import Vision

class CameraViewController: UIViewController {
    
    // MARK: - Properties
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    var viewModel: CameraViewModel?
    
    // MARK: - Methods

    func performVisionRequests(on pixelBuffer: CVPixelBuffer) {
        var requestOptions = [VNImageOption: Any]()
        if let cameraIntrinsicData = CMGetAttachment(
            pixelBuffer,
            key: kCMSampleBufferAttachmentKey_CameraIntrinsicMatrix,
            attachmentModeOut: nil
        ) {
            requestOptions = [.cameraIntrinsics: cameraIntrinsicData]
        }
        
        let handler = VNImageRequestHandler(
            cvPixelBuffer: pixelBuffer,
            orientation: .leftMirrored,
            options: requestOptions
        )
        
        let faceDetectionRequest = VNDetectFaceCaptureQualityRequest()
        
        do {
            try handler.perform([faceDetectionRequest])
            guard let faceObservations = faceDetectionRequest.results else {
                return
            }
            
            if viewModel?.isCapturingFaces == true {
                guard let observation = faceObservations.first else { return }
                
                viewModel?.saveImage(ciImg: CIImage(cvPixelBuffer: pixelBuffer), observation: observation)
            }
        } catch {
            print("Vision error: \(error.localizedDescription)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let newSession = makeAVCaptureSession() else {
            return
        }
        let previewLayer = AVCaptureVideoPreviewLayer(session: newSession)
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.insertSublayer(previewLayer, at: 0)
        self.previewLayer = previewLayer
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.global().async { [weak self] in
            self?.previewLayer?.session?.startRunning()
        }
        view.setNeedsLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        previewLayer?.session?.stopRunning()
        super.viewDidDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let previewLayer = self.previewLayer else {
            return
        }
        previewLayer.frame = view.bounds
    }
    
    func makeAVCaptureSession() -> AVCaptureSession? {
        guard let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else {
            print("Error locating AVCaptureDevice.")
            return nil
        }
        guard let input = try? AVCaptureDeviceInput(device: device) else {
            print("Error creating AVCaptureDeviceInput.")
            return nil
        }
        
        let session = AVCaptureSession()
        guard session.canAddInput(input) else {
            print("Can not add AVCaptureDeviceInput.")
            return nil
        }
        session.addInput(input)
        
        let output = AVCaptureVideoDataOutput()
        output.alwaysDiscardsLateVideoFrames = true
        output.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: .userInteractive))
        guard session.canAddOutput(output) else {
            print("Can not add AVCaptureVideoDataOutput.")
            return nil
        }
        session.addOutput(output)
        return session
    }
}

extension CameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        performVisionRequests(on: pixelBuffer)
    }
}

struct CameraViewControllerRepresentable: UIViewControllerRepresentable {
    @ObservedObject var viewModel: CameraViewModel
    
    func makeUIViewController(context: Context) -> CameraViewController {
        let viewController = CameraViewController()
        viewController.viewModel = viewModel
        return viewController
    }
    
    func updateUIViewController(_ uiViewController: CameraViewController, context: Context) {
        // Update the view controller when SwiftUI state changes
    }
}
