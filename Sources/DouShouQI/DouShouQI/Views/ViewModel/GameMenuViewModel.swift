//
//  GameMenuViewModel.swift
//  DouShouQI
//
//  Created by Loris Perret on 29/05/2024.
//

import Foundation
import DouShouQiModel
import SwiftUI

struct GameMenuItem: Equatable {
    var playerOneName: String = "Player1"
    var playerTwoName: String = "Player2"
    var playerOneType: Player.PlayerType = .human
    var playerTwoType: Player.PlayerType = .human
    var rulesType: RulesType = .classic
}

class GameMenuViewModel: ObservableObject {
    
    // MARK: - Properties

    @Published var game: GameMenuItem = GameMenuItem()
    
    private var existingOccurence: [Board : Int] = [:]
    private var existingHistoric: [Move] = []
    
    var errorReason: LocalizedStringKey = ""

    // MARK: - Methods

    func initGame(withGame game: Game) {
        guard let player1 = game.players[.player1],
              let player2 = game.players[.player2] else { return }
        
        self.game = GameMenuItem(
            playerOneName: player1.name,
            playerTwoName: player2.name,
            playerOneType: player1.playerType,
            playerTwoType: player2.playerType,
            rulesType: game.rules.type
        )
        
        if !game.rules.occurences.isEmpty {
            existingOccurence = game.rules.occurences
        }
        
        if !game.rules.historic.isEmpty {
            existingHistoric = game.rules.historic
        }
    }
    
    func launchGame() -> Game? {
        let rules = self.game.rulesType.instance(occurences: existingOccurence, historic: existingHistoric)
        let player1 = self.game.playerOneType.instance(withName: self.game.playerOneName, andId: .player1)
        let player2 = self.game.playerTwoType.instance(withName: self.game.playerTwoName, andId: .player2)
        
        guard let player1, let player2 else {
            errorReason = GameError.invalidPlayer.label
            return nil
        }
        
        do {
            let game = try Game(withRules: rules, andPlayer1: player1, andPlayer2: player2)
            return game
        } catch let error as GameError {
            errorReason = error.label
        } catch {
            errorReason = R.string.gameErrorUnknown
        }
        
        return nil
    }
}
