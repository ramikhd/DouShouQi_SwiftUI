//
//  TakePicturePlayerView.swift
//  DouShouQI
//
//  Created by Loris Perret on 11/06/2024.
//

import SwiftUI
import AVFoundation
import DouShouQiModel

class TakePicturePlayerViewModel {
    
    func nextState(game: Game) -> CameraViewModel.StatePicture {
        if nextPlayer2(game: game) {
            return .player2
        }
        
        return .finish
    }
    
    func initialState(game: Game) -> CameraViewModel.StatePicture {
        if nextPlayer1(game: game) {
            return .player1
        }
        
        return .player2
    }
    
    private func nextPlayer1(game: Game) -> Bool {
        game.players[.player1]?.playerType == .human
    }
    
    private func nextPlayer2(game: Game) -> Bool {
        game.players[.player2]?.playerType == .human
    }
    
    
}

struct TakePicturePlayerView: View {
    
    // MARK: - Properties

    @EnvironmentObject private var appState: AppState
    @StateObject private var viewModel = CameraViewModel()
    @State private var picturePlayer1: UIImage?
    @State private var picturePlayer2: UIImage?
    private var takePictureViewModel = TakePicturePlayerViewModel()
    
    var game: Game
    var isSpriteKit: Bool
    
    // MARK: - Initializer
    
    init(game: Game, isSpriteKit: Bool) {
        self.game = game
        self.isSpriteKit = isSpriteKit
    }

    // MARK: - Body

    var body: some View {
        VStack {
            switch viewModel.state {
            case .player1:
                player1
            case .player2:
                player2
            case .finish:
                finish
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .customPadding(.bottom, .xl)
        .background(R.color.background)
        .environmentObject(viewModel)
        .onAppear {
            viewModel.state = takePictureViewModel.initialState(game: game)
        }
    }
    
    // MARK: - Subviews

    private var player1: some View {
        VStack {
            Text(R.string.playerOnePicture)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
            
            TakePictureView(
                image: $picturePlayer1,
                nextState: takePictureViewModel.nextState(game: game)
            )
        }
    }
    
    private var player2: some View {
        VStack {
            Text(R.string.playerTwoPicture)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
            
            TakePictureView(image: $picturePlayer2, nextState: .finish)
        }
    }
    
    private var finish: some View {
        ScrollView {
            VStack {
                if let image = picturePlayer1 {
                    imageView(
                        image,
                        title: R.string.playerOnePicture,
                        color: appState.playerOneColor
                    )
                }
                
                if let image = picturePlayer2 {
                    imageView(
                        image,
                        title: R.string.playerTwoPicture,
                        color: appState.playerTwoColor
                    )
                }
                
                Button {
                    appState.currentState = .inGame(game: game, isSpriteView: isSpriteKit)
                } label: {
                    IconButtonView(
                        icon: R.image.check,
                        text: R.string.done
                    )
                }
                
                Button {
                    viewModel.state = .player1
                    appState.playerOnePicture = nil
                    appState.playerTwoPicture = nil
                } label: {
                    IconButtonView(
                        icon: R.image.xmark,
                        text: R.string.cancel
                    )
                }
            }
            .onAppear {
                appState.playerOnePicture = picturePlayer1
                appState.playerTwoPicture = picturePlayer2
            }
        }
        .customPadding(.l)
        .background(R.color.background)
        .edgesIgnoringSafeArea(.bottom)
    }
    
    private func imageView(
        _ image: UIImage,
        title: LocalizedStringKey,
        color: Color
    ) -> some View {
        VStack {
            Text(title)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
                .frame(maxWidth: .infinity, alignment: .leading)
            Image(uiImage: image)
                .resizable()
                .frame(width: 300, height: 300)
                .clipShape(Circle())
                .overlay(Circle().stroke(color, lineWidth: 5))
                .customPadding(.xs)
        }
    }
}

#Preview {
    TakePicturePlayerView(game: Game.Mock.previewGame, isSpriteKit: true)
}
