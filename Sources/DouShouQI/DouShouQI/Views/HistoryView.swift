//
//  HistoryView.swift
//  DouShouQI
//
//  Created by Rami Khedair on 19/05/2024.
//

import SwiftUI

struct HistoryView: View {
    
    // MARK: - Properties

    @StateObject private var viewModel = HistoricViewModel()
    
    // MARK: - Body

    var body: some View {
        VStack {
            historic(score: viewModel.getScore())
            historic(list: viewModel.getHistoric())
        }
        .customPadding(.l)
        .navigationTitle(R.string.historicTitle)
        .navigationBarTitleDisplayMode(.large)
        .background(R.color.background)
    }
    
    // MARK: - Subviews

    private func historic(list: [ResultItem]) -> some View {
        ScrollView {
            VStack {
                ForEach(list) { item in
                    ResultView(
                        resultItem: item
                    )
                }
            }
        }
        .customPadding(.top, .l)
    }
    
    private func historic(score: (victories: Int, defeats: Int)) -> some View {
        HStack(alignment: .bottom) {
            VStack {
                Text(R.string.historicVictory)
                Text("\(score.victories)")
            }
            
            Text(" - ")
            
            VStack {
                Text(R.string.historicDefeat)
                Text("\(score.defeats)")
            }
        }
        .text(.h1)
        .foregroundStyle(R.color.onTertiary)
        .frame(maxWidth: .infinity)
        .customPadding(.s)
        .background(R.color.dsqTertiary)
        .clipShape(RoundedRectangle(cornerRadius: 25.0))
    }
}

#Preview {
    HistoryView()
}
