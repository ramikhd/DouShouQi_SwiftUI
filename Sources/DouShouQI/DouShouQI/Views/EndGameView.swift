//
//  EndGameView.swift
//  DouShouQI
//
//  Created by Loris Perret on 05/06/2024.
//

import SwiftUI
import DouShouQiModel

struct EndGameView: View {

    // MARK: - Properties

    @EnvironmentObject private var appState: AppState
    let resultItem: ResultItem
    private let viewModel: EndGameViewModel

    // MARK: - Initializer

    init(game: Game, result: Result, time: Int) {
        self.resultItem = ResultItem(
            playerOne: game.players[.player1]!,
            playerTwo: game.players[.player2]!,
            result: result
        )
        self.viewModel = EndGameViewModel(game: game, time: time)
    }
    
    // MARK: - Body

    var body: some View {
        ScrollView {
            VStack(spacing: CustomPadding.xl.rawValue) {
                ResultView(
                    resultItem: resultItem
                )
                
                gameData
                
                playerData(player: resultItem.playerOne)
                playerData(player: resultItem.playerTwo)
                
                Button {
                    appState.currentState = .inNaviguation
                } label: {
                    IconButtonView(
                        icon: R.image.arrow,
                        text: R.string.done
                    )
                }
            }
        }
        .customPadding(.l)
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: .top
        )
        .background(R.color.background)
    }
    
    // MARK: - Subviews

    private var gameData: some View {
        VStack {
            Text(R.string.gameInfo)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
            
            HStack {
                Text(R.string.gameTime)
                Text(viewModel.getTime())
            }
            .text(.h2)
            .foregroundStyle(R.color.onTertiary)
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundStyle(R.color.onTertiary)
            .customPadding(.xl)
            .background(R.color.dsqTertiary)
            .clipShape(RoundedRectangle(cornerRadius: 25.0))
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private func playerData(player: Player) -> some View {
        VStack {
            Text(player.name)
                .text(.h1)
            
            VStack(spacing: CustomPadding.l.rawValue) {
                HStack {
                    Text(R.string.playerNumberMove)
                    Text("\(viewModel.getMoves(forPlayer: player))")
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                
                HStack {
                    Text(R.string.playerNumberPiece)
                    Text("\(viewModel.getPieces(forPlayer: player))")
                }
                .frame(maxWidth: .infinity, alignment: .leading)
            }
            .text(.h2)
            .foregroundStyle(R.color.onTertiary)
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundStyle(R.color.onTertiary)
            .customPadding(.xl)
            .background(R.color.dsqTertiary)
            .clipShape(RoundedRectangle(cornerRadius: 25.0))
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .foregroundStyle(R.color.dsqTertiary)
    }
}

#Preview("Even") {
    EndGameView(
        game: Game.Mock.previewGame,
        result: .even,
        time: 10
    )
}

#Preview("Not finished") {
    EndGameView(
        game: Game.Mock.previewGame,
        result: .notFinished,
        time: 10
    )
}

#Preview("Winner 1") {
    EndGameView(
        game: Game.Mock.previewGame,
        result: .winner(winner: .player1, reason: .denReached),
        time: 10
    )
}

#Preview("Winner 2") {
    EndGameView(
        game: Game.Mock.previewGame,
        result: .winner(winner: .player2, reason: .denReached),
        time: 10
    )
}
