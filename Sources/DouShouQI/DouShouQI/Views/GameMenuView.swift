//
//  GameMenuView.swift
//  DouShouQI
//
//  Created by Loris Perret on 23/05/2024.
//

import SwiftUI
import DouShouQiModel

struct GameMenuView: View {
    
    // MARK: - Properties

    @EnvironmentObject private var appState: AppState
    @StateObject private var viewModel = GameMenuViewModel()
    @State private var isEditingSheetPresented: Bool = false
    @State private var playerOne: Bool = false
    @State private var playerTwo: Bool = false
    @State private var showError: Bool = false
    @State private var game: Game?
    
    // MARK: - Initializer
    
    init() {}

    init(withGame game: Game) {
        viewModel.initGame(withGame: game)
    }

    // MARK: - Body

    var body: some View {
        ScrollView {
            content
        }
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    editButton
                }
            }
            .frame(
                maxWidth: .infinity,
                maxHeight: .infinity,
                alignment: .top
            )
            .customPadding(.xxl)
            .background(R.color.background)
            .sheet(isPresented: $isEditingSheetPresented) {
                GameMenuEditingSheetView(
                    initialGame: $viewModel.game,
                    isPresented: $isEditingSheetPresented
                )
            }
            .onChange(of: viewModel.game) {
                game = viewModel.launchGame()
            }
            .onAppear {
                game = viewModel.launchGame()
            }
            .navigationTitle(R.string.gameSettingsTitle)
            .navigationBarTitleDisplayMode(.large)
    }
    
    // MARK: - Subviews
    
    private var content: some View {
        VStack(spacing: CustomPadding.xxl.rawValue) {
            rules
            
            players
            
            playButtons
        }
    }
    
    private var playButtons: some View {
        HStack {
            playButton(title: R.string.menuPlay, isSpriteKit: true)
            playButton(title: R.string.menuPlayArKit, isSpriteKit: false)
        }
        .confirmationDialog(R.string.ok, isPresented: $showError) {
            Button(R.string.ok) { showError = false }
        } message: {
            Text(viewModel.errorReason)
        }
    }
    
    @ViewBuilder
    private func playButton(title: LocalizedStringKey, isSpriteKit: Bool) -> some View {
        if let game {
            if game.players.contains(where: { (key: Owner, value: Player) in
                value.playerType == .human
            }) {
                NavigationLink {
                    TakePicturePlayerView(game: game, isSpriteKit: isSpriteKit)
                } label: {
                    IconButtonView(
                        icon: R.image.play,
                        text: title,
                        backgroundColor: R.color.dsqPrimary,
                        foregroundColor: R.color.onPrimary
                    )
                }
            } else {
                Button {
                    appState.currentState = .inGame(game: game, isSpriteView: isSpriteKit)
                } label: {
                    IconButtonView(
                        icon: R.image.play,
                        text: title,
                        backgroundColor: R.color.dsqPrimary,
                        foregroundColor: R.color.onPrimary
                    )
                }
            }
        } else {
            Button {
                showError.toggle()
            } label: {
                IconButtonView(
                    icon: R.image.play,
                    text: title,
                    backgroundColor: R.color.dsqTertiary,
                    foregroundColor: R.color.onTertiary
                )
            }
        }
    }
    
    private var editButton: some View {
        Button {
            isEditingSheetPresented.toggle()
        } label: {
            R.image.edit
                .resizable()
                .renderingMode(.template)
                .foregroundStyle(R.color.dsqTertiary)
                .frame(width: 48, height: 48)
        }
    }

    private var rules: some View {
        VStack(spacing: CustomPadding.m.rawValue) {
            Text(R.string.gameSettingsRules)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Text(viewModel.game.rulesType.title)
                .text(.h1)
                .foregroundStyle(R.color.onTertiary)
                .customPadding(.xxl)
                .frame(maxWidth: .infinity)
                .background(R.color.dsqTertiary)
                .clipShape(RoundedRectangle(cornerRadius: 25.0))
        }
        
    }
    
    private var players: some View {
        VStack(spacing: CustomPadding.m.rawValue) {
            Text(R.string.gameSettingsPlayers)
                .text(.h1)
                .foregroundStyle(R.color.dsqTertiary)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            ExpandableMenu(
                title: R.string.gameSettingsPlayersOne,
                isOpen: $playerOne
            ) {
                VStack {
                    player(
                        title: R.string.gameSettingsPlayersName,
                        value: LocalizedStringKey(viewModel.game.playerOneName)
                    )
                    player(
                        title: R.string.gameSettingsPlayersType,
                        value: viewModel.game.playerOneType.label
                    )
                }
            }
            
            ExpandableMenu(
                title: R.string.gameSettingsPlayersTwo,
                isOpen: $playerTwo
            ) {
                VStack {
                    player(
                        title: R.string.gameSettingsPlayersName,
                        value: LocalizedStringKey(viewModel.game.playerTwoName)
                    )
                    player(
                        title: R.string.gameSettingsPlayersType,
                        value: viewModel.game.playerTwoType.label
                    )
                }
            }
        }
    }
    
    private func player(
        title: LocalizedStringKey,
        value: LocalizedStringKey
    ) -> some View {
        VStack {
            HStack(spacing: CustomPadding.xl.rawValue) {
                Text(title)
                Text(value)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .text(.h1)
        }
        .foregroundStyle(R.color.onTertiary)
        .customPadding(.l)
    }
}

#Preview {
    GameMenuView()
        .environmentObject(AppState.shared)
}
