//
//  SavesView.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import SwiftUI

struct SavesView: View {
    
    // MARK: - Properties

    @EnvironmentObject private var appState: AppState
    
    @StateObject private var viewModel = SavesViewModel()
    
//    @State private var isEditingSheetPresented: Bool = false
//    @State private var selectedSaved: SavedGameItem?
    
    // MARK: - Body

    var body: some View {
        VStack {
            parties(list: viewModel.getSavedParties())
        }
        .customPadding(.l)
        .background(R.color.background)
        .navigationTitle(R.string.savesTitle)
        .navigationBarTitleDisplayMode(.large)
//        .sheet(isPresented: $isEditingSheetPresented) {
//            if selectedSaved != nil {
//                SavedPartyEditView(party: $selectedSaved)
//            } else {
//                Text("Erreur")
//            }
//        }
    }
    
    // MARK: - Subviews

    private func parties(list: [SavedGameItem]) -> some View {
        List {
            ForEach(list) { item in
                party(item: item)
            }
            .onDelete(perform: deleteSave)
            .listRowBackground(Color.clear)
            .listRowSeparator(.hidden)
        }
        .scrollContentBackground(.hidden)
        .customPadding(.top, .l)
    }
    
    private func party(item: SavedGameItem) -> some View {
        HStack {
            Text(item.formattedDate)
                .text(.h2)
                .foregroundStyle(R.color.onTertiary)
                .frame(maxWidth: .infinity, alignment: .leading)
            
//            Button {
//                selectedSaved = item
//                isEditingSheetPresented.toggle()
//            } label: {
//                R.image.edit
//                    .resizable()
//                    .renderingMode(.template)
//                    .frame(width: 48, height: 48)
//                    .foregroundStyle(R.color.onTertiary)
//            }
//            .customPadding(.trailing, .s)
            
            NavigationLink {
                GameMenuView(withGame: item.game)
            } label: {
                R.image.play
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: 48, height: 48)
                    .foregroundStyle(R.color.onTertiary)
            }
            .frame(width: 48, height: 48)
        }
        .frame(maxWidth: .infinity)
        .customPadding(.m)
        .background(R.color.dsqTertiary)
        .clipShape(RoundedRectangle(cornerRadius: 25.0))
    }
    
    // MARK: - Methods

    private func deleteSave(at offsets: IndexSet) {
        
    }
}

#Preview {
    SavesView()
        .environmentObject(AppState.shared)
}
