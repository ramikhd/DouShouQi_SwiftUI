//
//  MenuView.swift
//  DouShouQI
//
//  Created by Loris Perret on 13/05/2024.
//

import SwiftUI
import DouShouQiModel

struct IdentifiableImage: Identifiable {
    let id = UUID()
    let image: Image
}

struct MenuView: View {
    
    // MARK: - Properties

    private let leftImages: [IdentifiableImage] = [
        IdentifiableImage(image: R.image.lion),
        IdentifiableImage(image: R.image.elephant),
        IdentifiableImage(image: R.image.wolf),
        IdentifiableImage(image: R.image.dog)
    ]
    
    private let rightImages: [IdentifiableImage] = [
        IdentifiableImage(image: R.image.tiger),
        IdentifiableImage(image: R.image.rat),
        IdentifiableImage(image: R.image.leopard),
        IdentifiableImage(image: R.image.cat)
    ]
    
    @EnvironmentObject private var appState: AppState
    
    // MARK: - Body
    
    var body: some View {
        NavigationStack {
            ZStack {
                HStack {
                    column(images: leftImages, alignment: .leading, rotation: -20)
                    column(images: rightImages, alignment: .trailing, rotation: 20)
                }
                VStack(spacing: CustomPadding.xxl.rawValue) {
                    image
                    buttons
                }
                .frame(
                    maxWidth: .infinity,
                    maxHeight: .infinity,
                    alignment: .top
                )
                .customPadding(.m)
            }
            .background(R.color.background)
            .toolbar {
                settingsButton
            }
        }
        .tint(R.color.dsqTertiary)
    }
    
    // MARK: - Subviews
    
    private var buttons: some View {
        VStack(spacing: CustomPadding.s.rawValue) {
            NavigationLink {
                GameMenuView()
            } label: {
                IconButtonView(
                    icon: R.image.play,
                    text: R.string.menuStart,
                    backgroundColor: R.color.dsqPrimary
                )
            }
            
            // Attendre la persistance des parties
            
//            NavigationLink {
//                HistoryView()
//            } label: {
//                IconButtonView(
//                    icon: R.image.historic,
//                    text: R.string.menuHistoric
//                )
//            }
//            
//            NavigationLink {
//                SavesView()
//            } label: {
//                IconButtonView(
//                    icon: R.image.saves,
//                    text: R.string.menuSaves
//                )
//            }
        }
    }
    
    private func column(
        images: [IdentifiableImage],
        alignment: Alignment,
        rotation: CGFloat
    ) -> some View {
        VStack {
            ForEach(images) { idImage in
                idImage.image.menuPageImage(rotation: rotation)
            }
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: alignment
        )
        .padding()
    }
    
    private var image: some View {
        R.image.logo
            .resizable()
            .aspectRatio(contentMode: .fit)
            .customPadding(.horizontal, 175)
            .customPadding(.vertical, .l)
    }
    
    private var settingsButton: some View {
        NavigationLink {
            SettingsView()
        } label: {
            R.image.settings
                .resizable()
                .renderingMode(.template)
                .foregroundStyle(R.color.dsqTertiary)
                .frame(width: 48, height: 48)
                .customPadding(.s)
        }
    }
}

#Preview {
    MenuView()
}
