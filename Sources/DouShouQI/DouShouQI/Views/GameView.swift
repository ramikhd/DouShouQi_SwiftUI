//
//  GameView.swift
//  DouShouQI
//
//  Created by Loris Perret on 21/05/2024.
//

import SwiftUI
import DouShouQiModel
import SpriteKit

struct GameView: View {
    
    // MARK: - Properties
    
    @EnvironmentObject private var appState: AppState
    @StateObject private var viewModel: GameViewModel
    @State private var actionSheetIsPresented: Bool = false
    @State private var isQuitShow = false
    @State private var isInfoShow = false
    private var isSpritesView: Bool
    
    // MARK: - Initializer

    init(game: Game, isSpriteView: Bool = true) {
        self.isSpritesView = isSpriteView
        if isSpriteView {
            _viewModel = StateObject(wrappedValue: GameSpriteKitViewModel(withGame: game))
        } else {
            _viewModel = StateObject(wrappedValue: GameARKitViewModel(withGame: game))
        }
    }
    
    // MARK: - Body
    
    var body: some View {
        ZStack{
            buttons
            .zIndex(Double(TopZIndex.indexMiddle))
            
            player(
                player: viewModel.player2,
                needRotation: true
            )
            .zIndex(Double(TopZIndex.indexMiddle))
            
            gameView
                .zIndex(Double(TopZIndex.indexBottom))
            
            animation
                .zIndex(Double(TopZIndex.indexTop))
            
            player(
                player: viewModel.player1,
                needRotation: false
            )
            .zIndex(Double(TopZIndex.indexMiddle))
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background {
            LinearGradient(
                gradient: Gradient(colors: viewModel.colors),
                startPoint: .bottom,
                endPoint: .top
            )
            .ignoresSafeArea()
        }
    }
    
    // MARK: - Subviews
    
    @ViewBuilder
    private var animation: some View {
        switch viewModel.state {
        case .invalidMove:
            LottieWithBackgroundView(animation: viewModel.state.file) {
                viewModel.state = .none
            }
        case .startGame:
            LottieWithBackgroundView(animation: viewModel.state.file) {
                Task {
                    if isSpritesView {
                        await viewModel.gameScene.startGame()
                    }
                    viewModel.state = .none
                }
            }
        case .endGame(let file, let color):
            LottieWithBackgroundView(
                color: color,
                animation: file
            ) {
                appState.currentState = .finishGame(
                    game: viewModel.game,
                    result: viewModel.gameResult,
                    time: viewModel.gameTime
                )
            }
        case .none:
            EmptyView()
        }
    }
    
    private var gameView: some View {
        Group {
            if isSpritesView {
                SpriteView(scene: viewModel.gameScene, options: [.allowsTransparency])
                    .onAppear {
                        viewModel.gameScene.beforeStart()
                        viewModel.state = .startGame
                    }
            } else {
                ARViewContainer(game: viewModel.game)
                    .onAppear {
                        viewModel.state = .startGame
                    }
            }
        }
        .customPadding(.xxl)
    }
    
    private func player(player: Player, needRotation: Bool) -> some View {
        HStack {
            Button{
                if isSpritesView {
                    viewModel.gameScene.giveAllHint(forPlayer: player.id)
                }
            } label: {
                Text(player.name)
                    .text(.h1)
                    .foregroundStyle(viewModel.currentPlayer != player.id ? Color.gray : R.color.forground)
                    .customPadding(.horizontal, .xxl)
                    .customPadding(.vertical, .s)
                    .background(R.color.background)
                    .clipShape(RoundedRectangle(cornerRadius: 25.0))
            }
            .disabled(viewModel.currentPlayer != player.id)
        }
        .frame(maxHeight: .infinity, alignment: .bottom)
        .rotationEffect(needRotation ? .degrees(180) : .zero)
        .customPadding(.s)
    }
    
    private var buttons: some View {
        HStack {
            infoButton
            
            Spacer()
            
            quitButton
        }
        .customPadding(.horizontal, .m)
    }
    
    private var infoButton: some View {
        Button {
            isInfoShow = true
        } label: {
            R.image.questionmark
                .resizable()
                .renderingMode(.template)
                .foregroundStyle(R.color.onTertiary)
                .frame(width: 16, height: 20)
                .customPadding(.s)
                .background(R.color.dsqTertiary)
                .clipShape(Circle())
        }
        .sheet(isPresented: $isInfoShow) {
            InfoPiecesSheetView(animalList: viewModel.availableAnimalList)
        }
    }
    
    private var quitButton: some View {
        Button {
            isQuitShow = true
        } label: {
            R.image.xmarkSimple
                .resizable()
                .renderingMode(.template)
                .foregroundStyle(R.color.onTertiary)
                .frame(width: 16, height: 16)
                .customPadding(.s)
                .background(R.color.dsqTertiary)
                .clipShape(Circle())
        }
        .alert(
            R.string.gameQuitTitle,
            isPresented: $isQuitShow
        ) {
            Button(R.string.gameQuitYes, role: .destructive) {
                viewModel.gameTime = abs(Int(viewModel.startTime.timeIntervalSinceNow.rounded()))
                appState.currentState = .finishGame(
                    game: viewModel.game,
                    result: viewModel.gameResult,
                    time: viewModel.gameTime
                )
            }
            Button(R.string.gameQuitNo, role: .cancel) { }
        }
    }
}


#Preview {
    GameView(game: Game.Mock.previewGame)
        .environmentObject(AppState.shared)
}
