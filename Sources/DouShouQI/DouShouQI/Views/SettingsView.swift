//
//  SettingsView.swift
//  DouShouQI
//
//  Created by Loris Perret on 15/05/2024.
//

import SwiftUI

struct SettingsView: View {
    
    // MARK: - Properties

    @EnvironmentObject private var appState: AppState

    // MARK: - Body

    var body: some View {
        VStack(spacing: CustomPadding.xxl.rawValue) {
            CustomPickerView(
                R.string.settingsLanguage,
                selection: $appState.appLanguage,
                items: Languages.allCases
            )
            
            CustomPickerView(
                R.string.settingsColorScheme,
                selection: $appState.appColorScheme,
                items: AppColorScheme.allCases
            )
            
            playersColor
        }
        .customPadding(.xl)
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: .top
        )
        .background(R.color.background)
        .navigationTitle(R.string.settingsTitle)
        .navigationBarTitleDisplayMode(.large)
    }
    
    // MARK: - Subviews

    private var playersColor: some View {
        VStack {
            HStack {
                Text(R.string.settingsPlayerOneColor)
                    .text(.h1)
                    .foregroundStyle(R.color.dsqTertiary)
                ColorPicker("", selection: $appState.playerOneColor)
                    .frame(maxWidth: 50)
            }
            
            HStack {
                Text(R.string.settingsPlayerTwoColor)
                    .text(.h1)
                    .foregroundStyle(R.color.dsqTertiary)
                ColorPicker("", selection: $appState.playerTwoColor)
                    .frame(maxWidth: 50)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}

#Preview {
    SettingsView()
        .environmentObject(AppState.shared)
}
