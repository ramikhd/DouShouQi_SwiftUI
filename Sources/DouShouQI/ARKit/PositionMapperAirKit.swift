//
//  PositionMapperAirKit.swift
//  DouShouQI
//
//  Created by Rami Khedair on 12/06/2024.
//

import Foundation

struct PositionMapperAirKit {
    static var offset = CGPoint(x: direction.dx * -3, y:  direction.dy * 4)
    static let direction = CGVector(dx: 0.0391891892, dy: 0.0391891892)
    static var maxValue = CGPoint(x: 6, y: 8)

    static func realPosition(from point: CGPoint) -> CGPoint {
        CGPoint(
            x: CGFloat((Float(point.x) - Float(offset.x)) / Float(direction.dx)).rounded(),
            y: CGFloat((Float(point.y) - Float(offset.y)) / Float(-direction.dy)).rounded()
        )
    }
    
    static func realPosition(x : Int ,y : Int) -> CGPoint {
        return realPosition(from: CGPoint(x: x, y: y))
    }
        
    static func transformePosition(x : Int ,y : Int) -> SIMD3<Float> {
        return transformePosition(point: CGPoint(x: x, y: y))
    }
    
    static func transformePosition(point : CGPoint) -> SIMD3<Float> {
        return SIMD3<Float>(
            (Float(direction.dx) * Float(point.x)) + Float(offset.x),
            0,
            (Float(direction.dy) * Float(-point.y)) + Float(offset.y)
        )
    }
    
}
