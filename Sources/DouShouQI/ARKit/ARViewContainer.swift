//
//  ARViewContainer.swift
//  DouShouQI
//
//  Created by Loris Perret on 03/06/2024.
//

import SwiftUI
import RealityKit
import ARKit
import DouShouQiModel

struct ARViewContainer: UIViewRepresentable {
    
    let game: Game
    
    func makeUIView(context: Context) -> ARView {
        ARKitView(game: game)
    }
    
    func updateUIView(_ uiView: ARView, context: Context) {}
    
}
