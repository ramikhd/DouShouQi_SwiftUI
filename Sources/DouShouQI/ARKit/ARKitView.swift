//
//  ARKitView.swift
//  DouShouQI
//
//  Created by Loris Perret on 03/06/2024.
//

import SwiftUI
import RealityKit
import ARKit
import DouShouQiModel

class ARKitView: ARView {
    var boardImage: AnchorEntity
    var pieces: [Owner: [PieceCosmonaut]] = [
        .player1 : [],
        .player2 : []
    ]
    var game: Game
    var currentPlayer : Player
    
    private func addImageToScene(game: Game) -> AnchorEntity {
        let scale = 0.0003918919
        let boardSize = game.rules.boardSize
        let planeMesh = MeshResource.generatePlane(
            width: Float(boardSize.width * scale),
            depth: Float(boardSize.height * scale)
        )
        
        guard let texture = try? TextureResource.load(named: game.rules.boardImageName) else {
            fatalError("Failed to load the texture resource.")
        }
       
        var material = SimpleMaterial()
        material.baseColor = .texture(texture)
       
        let modelEntity = ModelEntity(mesh: planeMesh, materials: [material])
        modelEntity.generateCollisionShapes(recursive: true)
        
        let anchorEntity = AnchorEntity(.plane(.horizontal, classification: .table, minimumBounds: SIMD2<Float>(0.3, 0.4)))

        anchorEntity.addChild(modelEntity)
        return anchorEntity
    }
    
    func beforeStart() {
        PositionMapperAirKit.offset = CGPoint(
            x: PositionMapperAirKit.direction.dx * game.rules.offset.x/100,
            y:  PositionMapperAirKit.direction.dy * game.rules.offset.y/(-100))

        for row in 0..<game.board.grid.count {
            for col in 0..<game.board.grid[row].count {
                if let piece = game.board.grid[row][col].piece {
                    let pieceCosmonaut = PieceCosmonaut(piece: piece,parent: boardImage)
                    pieces[piece.owner]?.append(pieceCosmonaut)
                    addCosmonaut(piece: pieceCosmonaut)
                    pieceCosmonaut.cellPosition = CGPoint(x: col, y: row)
                }
            }
        }
        self.scene.addAnchor(boardImage)
    }

    convenience init(game: Game) {
        self.init(frame: UIScreen.main.bounds)
        self.game = game
        self.boardImage = addImageToScene(game: game)
        
        PieceCosmonaut.addMoveListner(listner: moveWasChosen)
        beforeStart()

        self.game.addPlayerNotifiedListener(playerNotifiedListener)
        self.game.addInvalidMoveCallbacksListener(invalidMoveCallbacksListener)
        self.game.addMoveChosenCallbacksListener(moveChosenCallbacksListener)

        currentPlayer = self.game.players[.player1]!

        Task{
            try! await self.game.start()
        }
    }
    
    func moveChosenCallbacksListener(
        board: Board,
        move: Move,
        player: Player
    ) {
        guard let openentPieces = pieces[!currentPlayer.id] else { return }
        guard let playerPieces = self.pieces[currentPlayer.id] else { return }

        if !(currentPlayer is HumanPlayer) {
            guard let piece = board.grid[move.rowOrigin][move.columnOrigin].piece,
                  let chosenPiece = playerPieces.first(where: { $0.piece == piece }) else { return }
            
            var transform = chosenPiece.modelEntity.transform
            let newPosition = PositionMapperAirKit.transformePosition(x: move.columnDestination, y: move.rowDestination)
            transform.translation = newPosition
            
            chosenPiece.modelEntity.move(
                to: transform,
                relativeTo: chosenPiece.modelEntity.parent,
                duration: 3
            )
        }
        
        guard game.rules.isMoveValid(onBoard: game.board, withMove: move) else {
            return
        }

        if currentPlayer is HumanPlayer {
            for pieceNode in playerPieces {
                pieceNode.canMove = false
            }
        }
        
        guard let piece = board.grid[move.rowDestination][move.columnDestination].piece,
              let openentPiece = openentPieces.first(where: { $0.piece == piece }) else { return }
        
        boardImage.removeChild(openentPiece.modelEntity)

    }
    
    func playerNotifiedListener(board: Board, player: Player) async {
        guard let playerPieces = self.pieces[player.id] else { return }
        currentPlayer = player
        if currentPlayer is HumanPlayer {
            for pieceNode in playerPieces {
                pieceNode.canMove = true
            }
        } else {
            try! await currentPlayer.chooseMove(in: board, with: self.game.rules)
        }
    }
    
    func invalidMoveCallbacksListener(
        board: Board,
        move: Move,
        player: Player,
        result: Bool
    ) {
        if result {
            return
        }
        
        guard let playerPieces = self.pieces[player.id] else { return }
        
        let pieceNode = playerPieces.first(where: { pieceNode in
            let cell = self.game.board.grid[move.rowOrigin][move.columnOrigin]
            return pieceNode.piece == cell.piece
        })
        
        if let pieceNode {
            pieceNode.cellPosition = CGPoint(x: move.columnOrigin, y: move.rowOrigin)
        }
    }
    
    func moveWasChosen(move: Move){
        debugPrint(currentPlayer, move)
        Task {
            if let player = currentPlayer as? HumanPlayer {
                try! await player.chooseMove(move)
            }
        }
    }
    
    func addCosmonaut(piece : PieceCosmonaut) {
        boardImage.addChild(piece.modelEntity)
        self.installGestures([.all], for: piece.modelEntity as Entity & HasCollision).forEach { gestureRecognizer in
            gestureRecognizer.addTarget(piece, action: #selector(piece.handleGesture(_:)))
        }
    }
    
    func applyConfiguration() {
        session.run(ARWorldTrackingConfiguration())
    }
    
    required init(frame frameRect: CGRect) {
        self.game = Game.Mock.previewGame
        currentPlayer = game.players[.player1]!
        self.boardImage = AnchorEntity()
        super.init(frame: frameRect)
    }
    
    @MainActor
    required dynamic init?(coder decoder: NSCoder) {
        self.game = Game.Mock.previewGame
        currentPlayer = game.players[.player1]!
        self.boardImage = AnchorEntity()
        super.init(coder: decoder)
    }
    
}
