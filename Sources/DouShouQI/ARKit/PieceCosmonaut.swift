//
//  PieceCosmonaut.swift
//  DouShouQI
//
//  Created by Rami Khedair on 11/06/2024.
//

import Foundation
import RealityKit
import ARKit
import DouShouQiModel

class PieceCosmonaut {
    private static var moveListners: [(Move) -> Void] = []
    let piece: Piece
    let modelEntity : ModelEntity
    let parent : Entity
    
    var cellPosition: CGPoint {
        didSet(value) {
            modelEntity.setPosition(PositionMapperAirKit.transformePosition(point: cellPosition), relativeTo: parent)
        }
    }
    
    var lastCellPosition: CGPoint
    var canMove = false {
        didSet(value) {
            if canMove {
                modelEntity.generateCollisionShapes(recursive: false)
            } else {
                modelEntity.components.remove(CollisionComponent.self)
            }
        }
    }
    var wasClickedBefore = false
    
    init(piece: Piece, parent: Entity) {
        self.parent = parent
        self.piece = piece
        self.cellPosition = CGPoint(x: 0,y: 0)
        self.lastCellPosition = CGPoint(x: 0,y: 0)
        self.modelEntity = try! Entity.loadModel(named: piece.animal.imageName)
       
        modelEntity.model?.materials[0] = SimpleMaterial(color: UIColor(piece.owner.color), isMetallic: true)
        if piece.owner == .player1 {
            
            let zRotation = simd_quatf(angle: .pi, axis: SIMD3(x: 0, y: 0, z: 1))

            let xCorrectionRotation = simd_quatf(angle: (90 * .pi / 180), axis: SIMD3(x: 1, y: 0, z: 0))

            modelEntity.orientation = simd_mul(zRotation, xCorrectionRotation)
            //modelEntity.orientation += simd_quatf(angle: radians, axis: SIMD3(x: 1, y: 0, z: 0))

        }
        //modelEntity.generateCollisionShapes(recursive: false)
        modelEntity.setScale(SIMD3<Float>(0.3, 0.3, 0.3), relativeTo: parent)

    }
    
    public func moveBegan(){
        lastCellPosition = cellPosition
    }
    
    public func moveEnded(){
        let newPosition = PositionMapperAirKit.realPosition(from:
                        CGPoint(
                            x: CGFloat(modelEntity.position.x) ,
                            y: CGFloat(modelEntity.position.z)
                    ))
        
        let confirmedPosition = CGPoint(
            x: newPosition.x > PositionMapperAirKit.maxValue.x ? PositionMapperAirKit.maxValue.x  : newPosition.x ,
            y: newPosition.y > PositionMapperAirKit.maxValue.y ? PositionMapperAirKit.maxValue.y  : newPosition.y
        )
         
        self.cellPosition = confirmedPosition

        guard lastCellPosition != confirmedPosition else { return }
        
        PieceCosmonaut.moveListners.forEach{listner in
            listner(
                Move(
                    of: piece.owner,
                    fromRow: Int(lastCellPosition.y),
                    andFromColumn: Int(lastCellPosition.x),
                    toRow: Int(cellPosition.y),
                    andToColumn: Int(cellPosition.x)
                )
            )
        }
    }
    
    func addHandeller(arView : ARView){
        arView.installGestures([.all], for: modelEntity as Entity & HasCollision).forEach { gestureRecognizer in
            gestureRecognizer.addTarget(self, action: #selector(handleGesture(_:)))
        }
    }
    
    static func addMoveListner(listner: @escaping (Move) -> Void){
        PieceCosmonaut.moveListners.append(listner)
    }
    
    @objc func handleGesture(_ recognizer: UIGestureRecognizer) {
        guard let translationGesture = recognizer as? EntityTranslationGestureRecognizer, let entity = translationGesture.entity else { return }
            
            switch translationGesture.state {
            case .began:
                moveBegan()
                
            case .ended:
                moveEnded()
            default:
                break
        }
    }
    
}
