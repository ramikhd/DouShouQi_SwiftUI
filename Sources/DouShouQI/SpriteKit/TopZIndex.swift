//
//  TopZIndex.swift
//  DouShouQI
//
//  Created by Rami Khedair on 31/05/2024.
//

import Foundation

struct TopZIndex{
    static let indexTop = 3
    static let indexHintLines = 2
    static let indexMiddle = 1
    static let indexBottom = 0    
}
