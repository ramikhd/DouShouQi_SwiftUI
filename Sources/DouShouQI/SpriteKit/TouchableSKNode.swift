//
//  SKNode.swift
//  DouShouQI
//
//  Created by Rami Khedair on 30/05/2024.
//

import Foundation
import SpriteKit

class TouchableSKNode<PARAM> : SKNode{
    var clickedPieces: [(PARAM) -> Void] = []
    var clickedParam : PARAM?
    var moved = false
  
    override init() {
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
    func addPieceClicked(listner: @escaping (PARAM) -> Void){
        clickedPieces.append(listner)
    }
    
    func clicked(){
        guard let clickedParamNotNull = clickedParam else{
            fatalError("Must override clicked() and set clickedParam")
        }
        self.clickedPieces.forEach{listner in
            listner(clickedParamNotNull)
        }
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        moved = true
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard moved else {
           clicked()
           moved = false
           return
        }

    }

}
