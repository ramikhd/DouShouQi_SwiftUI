//
//  PositionMapper.swift
//  DouShouQI
//
//  Created by Loris Perret on 30/05/2024.
//

import Foundation

struct PositionMapper {
    static var offset = CGPoint(x: -300, y: -400)
    static let direction = CGVector(dx: 100, dy: 100)
    
    static func realPosition(from point: CGPoint) -> CGPoint {
        CGPoint(
            x: offset.x + direction.dx * point.x,
            y: offset.y + direction.dy * point.y
        )
    }
}
