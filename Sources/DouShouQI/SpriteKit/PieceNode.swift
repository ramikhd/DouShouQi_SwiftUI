//
//  PieceNode.swift
//  DouShouQI
//
//  Created by Rami Khedair on 27/05/2024.
//

import Foundation
import SpriteKit
import DouShouQiModel

class PieceNode: TouchableSKNode<PieceNode> {
    private static var moveListners: [(Move) -> Void] = []

    let piece: Piece
    let imageNode : SKSpriteNode
    let imageHemlet : SKSpriteNode
    
    var cellPosition: CGPoint {
        didSet(value) {
            self.position = PositionMapper.realPosition(from: cellPosition)
        }
    }
    
    var lastCellPosition: CGPoint
    var canMove = false
    var wasClickedBefore = false
    
    init(piece: Piece){
        self.piece = piece
        self.imageNode = SKSpriteNode(imageNamed: piece.animal.imageName)
        self.imageHemlet = SKSpriteNode(imageNamed: R.string.hemlet)

        self.imageNode.size = CGSize(width: 90, height: 90)
        self.imageHemlet.size = CGSize(width: 70, height: 70)
        self.imageHemlet.color = UIColor(self.piece.owner.color)
        self.imageHemlet.colorBlendFactor = 1
        
        self.cellPosition = CGPoint(x: 6,y: 0)
        self.lastCellPosition = CGPoint(x: 6,y: 0)
        super.init()

        self.addChild(imageNode)
        self.addChild(imageHemlet)
        
        var pad : CGFloat = 10
        
        if piece.owner == .player2 {
            imageNode.zRotation = .pi
            imageHemlet.zRotation = .pi
            pad = -pad
        }
        
        imageHemlet.position = CGPoint(
            x: imageHemlet.position.x,
            y: imageHemlet.position.y + pad
        )
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.piece = Piece(withOwner: .noOne, andAnimal: .cat)
        self.imageNode = SKSpriteNode(imageNamed: piece.animal.imageName)
        self.imageHemlet = SKSpriteNode(imageNamed: R.string.hemlet)
        self.cellPosition = CGPoint(x: 0,y: 0)
        self.lastCellPosition = CGPoint(x: 6,y: 0)
        
        super.init(coder: aDecoder)
    }
    
    override var isUserInteractionEnabled: Bool {
        set {}
        get { canMove }
    }
    
    static func addMoveListner(listner: @escaping (Move) -> Void){
        PieceNode.moveListners.append(listner)
    }
    
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.zPosition = CGFloat(TopZIndex.indexTop)
        lastCellPosition = cellPosition
        
    }
    
    override func clicked(){
        clickedParam = self
        super.clicked()
        wasClickedBefore = !wasClickedBefore
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        self.position = touches.first?.location(in: parent!) ?? CGPoint(x: 0, y: 0)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        // There is a move
        if let touch = touches.first {
            // Get new position
            let x = ((touch.location(in: parent!).x) - PositionMapper.offset.x).getLeadingDigits(count: 2)
            let y = ((touch.location(in: parent!).y) - PositionMapper.offset.y).getLeadingDigits(count: 2)

//          if x < 0 || y < 0 {
//              self.cellPosition = lastCellPosition
//          } else {}
                
            self.cellPosition = CGPoint(
                x: x,
                y: y
            )
            
            // The piece hasn't moved
            if cellPosition == lastCellPosition {
                moved = false
                return
            }
            
            // Move the piece
            PieceNode.moveListners.forEach{listner in
                listner(
                    Move(
                        of: piece.owner,
                        fromRow: Int(lastCellPosition.y),
                        andFromColumn: Int(lastCellPosition.x),
                        toRow: Int(cellPosition.y),
                        andToColumn: Int(cellPosition.x)
                    )
                )
            }
            self.zPosition = CGFloat(TopZIndex.indexMiddle)
            // Unlock the semaphore
            AppState.shared.semaphoreTurn.signal()
        }
        
        moved = false
    }
}
