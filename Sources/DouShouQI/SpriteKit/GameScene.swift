//
//  GameScene.swift
//  DouShouQI
//
//  Created by Rami Khedair on 23/05/2024.
//

import Foundation
import SpriteKit
import DouShouQiModel

class GameScene: SKScene {
    
    // MARK: - Properties
    
    var game: Game
    var pieces: [Owner: [PieceNode]] = [
        .player1 : [],
        .player2 : []
    ]
    
    var chosenMove: Move = Move(
        of: .noOne,
        fromRow: 0,
        andFromColumn: 0,
        toRow: 0,
        andToColumn: 0
    )
    
    // MARK: - Initializer

    required init?(coder aDecoder: NSCoder) {
        self.game = try! Game(withRules: ClassicRules(), andPlayer1: Player.Mock.player1, andPlayer2: IAPlayer(withName: "ee", andId: .player2)!)
        
        super.init(coder: aDecoder)
    }
    
    init(withGame game: Game, size: CGSize) {
        self.game = game
        
        super.init(size: size)
        
        self.backgroundColor = .clear
        self.scaleMode = .aspectFit
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        // Board Image
        let imageBoard = SKSpriteNode(imageNamed: game.rules.boardImageName)
        self.addChild(imageBoard)
        PositionMapper.offset = game.rules.offset
        
        
        // Listeners
        
        PieceNode.addMoveListner(listner: moveWasChosen(move:))

        game.addPlayerNotifiedListener(playerNotifiedListener)
        game.addInvalidMoveCallbacksListener(invalidMoveCallbacksListener)
        game.addMoveChosenCallbacksListener(moveChosenCallbacksListener)

        // Add pieces on board
        
    }
    
    // MARK: - Methods
    func beforeStart() {
        Task {
                for row in 0..<game.board.grid.count {
                    for col in 0..<game.board.grid[row].count {
                        if let piece = game.board.grid[row][col].piece {
                            let pieceNode = PieceNode(piece: piece)
                            
                            pieces[piece.owner]?.append(pieceNode)
                            pieceNode.zPosition = CGFloat(TopZIndex.indexMiddle)
                            self.addChild(pieceNode)
                            
                            let destination = CGPoint(x: col, y: row)
                            let moveAction = SKAction.move(to: PositionMapper.realPosition(from: destination), duration: 0.1)
                            
                            await withCheckedContinuation { continuation in
                                pieceNode.run(moveAction) {
                                    pieceNode.cellPosition = destination
                                    pieceNode.addPieceClicked(listner: self.pieceWasClicked)
                                    continuation.resume()
                                }
                            }
                        }
                    }
                }
            }
    }
    
    func startGame() async{
        try! await game.start()
    }
    
    func moveWasChosen(move : Move) -> Void {
        chosenMove = move
    }
    
    func pieceWasClicked(pieceNode : PieceNode) -> Void {
        clearHint()
        
        let row = Int(pieceNode.cellPosition.y)
        let column = Int(pieceNode.cellPosition.x)
        let moves = game.rules.getMoves(
            in: game.board,
            of: pieceNode.piece.owner,
            fromRow: row,
            andColumn: column
        )
        
        for move in moves {
            let squareSize = CGSize(width: 95, height: 95)
            let squareNode = SKShapeNode(rectOf: squareSize)
            let squareToTouch = SKShapeNode(rectOf: squareSize)
            squareToTouch.fillColor = .clear
            squareToTouch.zPosition = CGFloat(TopZIndex.indexTop)
            squareNode.zPosition = CGFloat(TopZIndex.indexBottom)

            squareNode.fillColor = UIColor(pieceNode.piece.owner.color.opacity(77))
            
       
            let touchableElement : TouchableElement<PieceNode> = TouchableElement(param: pieceNode)
            touchableElement.addChild(squareNode)
            touchableElement.addChild(squareToTouch)

            
            touchableElement.position = PositionMapper.realPosition(
                from: CGPoint(x: move.columnDestination, y: move.rowDestination)
            )
            touchableElement.name = "hint"
          
            touchableElement.addPieceClicked(listner: clickedSquare)
            self.run(SKAction.wait(forDuration: 0.1)) {
                self.addChild(touchableElement)
            }
        }
    }
    func clickedSquare(me: TouchableElement<PieceNode>){
        clearHint()
        let x = (me.position.x - PositionMapper.offset.x).getLeadingDigits(count: 2)
        let y = (me.position.y - PositionMapper.offset.y).getLeadingDigits(count: 2)
        chosenMove = Move(of: me.clickedParam.piece.owner, fromRow: Int(me.clickedParam.cellPosition.y), andFromColumn: Int(me.clickedParam.cellPosition.x), toRow: Int(y), andToColumn: Int(x))
        me.clickedParam.cellPosition = CGPoint(x: x, y: y)
        AppState.shared.semaphoreTurn.signal()

    }
    
    func clearHint() {
        self.run(SKAction.wait(forDuration: 0)) {
            for child in self.children {
                if child.name == "hint" {
                    child.removeFromParent()
                }
            }
        }
    }
    
    func giveAllHint(forPlayer player: Owner) {
        for move in game.rules.getMoves(in: game.board, of: player) {
            
            let startPoint =  PositionMapper.realPosition(from: CGPoint(x: move.columnOrigin, y: move.rowOrigin))
            var endPoint =  PositionMapper.realPosition(from: CGPoint(x: move.columnDestination, y: move.rowDestination))
            
            let path = CGMutablePath()
            
            // Add the shape node to the scene
            let circle = SKShapeNode(circleOfRadius: 10)
            circle.zPosition = CGFloat(TopZIndex.indexHintLines)
            circle.fillColor = UIColor(player.color)
            
            if startPoint.x == endPoint.x {
                if startPoint.y > endPoint.y {
                    endPoint.y = endPoint.y + circle.frame.height
                } else {
                    endPoint.y = endPoint.y - circle.frame.height
                }
                
            }
            if startPoint.y == endPoint.y{
                if startPoint.x > endPoint.x {
                    endPoint.x = endPoint.x + circle.frame.width
                } else {
                    endPoint.x = endPoint.x - circle.frame.width
                }
                
            }
            circle.position = endPoint
            path.move(to: startPoint)
            path.addLine(to: endPoint)
            let line = SKShapeNode(path: path)
            line.name = "hint"
            circle.name = "hint"
            
            line.strokeColor = UIColor(player.color)
            line.lineWidth = 4.0
            
            addChild(line)
            addChild(circle)
        }
    }
    
    // MARK: - Listeners

    func playerNotifiedListener(board: Board, player: Player) async{
        DispatchQueue.global().async {
            Task {
                guard player is HumanPlayer else {
                    try! await player.chooseMove(in: board, with: self.game.rules)
                    return
                }
                guard let playerPieces = await self.pieces[player.id] else { return }
                
                for pieceNode in playerPieces {
                    pieceNode.canMove = true
                }
                
                AppState.shared.semaphoreTurn.wait()
                
                for pieceNode in playerPieces {
                    pieceNode.canMove = false
                }
                
                try! await (player as! HumanPlayer).chooseMove(self.chosenMove)
            }
        }
    }
    
    func invalidMoveCallbacksListener(
        board: Board,
        move: Move,
        player: Player,
        result: Bool
    ) {
        if result {
            return
        }
        
        guard let playerPieces = self.pieces[player.id] else { return }
        
        let pieceNode = playerPieces.first(where: { pieceNode in
            let cell = self.game.board.grid[move.rowOrigin][move.columnOrigin]
            return pieceNode.piece == cell.piece
        })
        
        if let pieceNode {
            pieceNode.cellPosition = CGPoint(x: move.columnOrigin, y: move.rowOrigin)
        }
    }
    
    func moveChosenCallbacksListener(
        board: Board,
        move: Move,
        player: Player
    ) {
        guard let openentPieces = pieces[!player.id], let playerPieces = pieces[player.id],
              game.rules.isMoveValid(onBoard: game.board, withMove: move) else { return }
        
        if !(player is HumanPlayer) {
            guard let nodePiece = playerPieces.first(where: {nodePiece in
                return nodePiece.cellPosition.x == CGFloat(move.columnOrigin) && nodePiece.cellPosition.y == CGFloat(move.rowOrigin)
            }) else { return }
            let destination = CGPoint(x: move.columnDestination, y: move.rowDestination)
            let moveAction = SKAction.move(to: PositionMapper.realPosition(from: destination) , duration: 1)
            
            let group = DispatchGroup()
            group.enter()
            nodePiece.run(moveAction) {
                nodePiece.cellPosition = destination
                group.leave()
            }

            // Wait for the animation to finish
            group.wait()

            
        }
        
        clearHint()
        
        guard let piece = board.grid[move.rowDestination][move.columnDestination].piece,
              let openentNodepiece = openentPieces.first(where: { $0.piece == piece }) else { return }
        
        self.run(SKAction.wait(forDuration: 0.1)) {
            self.removeChildren(in: [openentNodepiece])
        }
    }
}
