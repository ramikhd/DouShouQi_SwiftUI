//
//  TouchableElement.swift
//  DouShouQI
//
//  Created by Rami Khedair on 31/05/2024.
//

import Foundation
import SpriteKit

class TouchableElement<PARAM> : SKNode{
    var clickedPieces: [(TouchableElement<PARAM>) -> Void] = []
    var clickedParam : PARAM
    var moved = false
  
    init(param : PARAM) {
        clickedParam = param
        super.init()
    }
    
    override var isUserInteractionEnabled: Bool {
        set {}
        get { true }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addPieceClicked(listner: @escaping (TouchableElement<PARAM>) -> Void){
        clickedPieces.append(listner)
    }
    
    func clicked(){
        self.clickedPieces.forEach{listner in
            listner(self)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        moved = true
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard moved else {
           clicked()
           moved = false
           return
        }

    }

}
