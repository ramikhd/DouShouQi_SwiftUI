# DouShouQi_SwiftUI

<br/><div align="center">
    <img  height="450" src="Images/logo.svg"/> 
</div><br/>

### Sommaire <br/>
[Introduction](#introduction) | [Équipe](#équipe) | [Sketch](#sketch)  |   [Architecture](#architecture)  | [Langues Disponibles](#langues-disponibles) | [Modes de Jeu](#modes-de-jeu) | [Technologies Utilisées](#technologies-utilisées)

<div id='introduction' />

## ✨ Introduction
Ce projet est une interface graphique au jeu du **DouShouQi**. Ce jeu disponible sur **iOS** est réalisé en **SwiftUI**.

<div id='équipe' />

## 👥 Équipe
Ce projet est réalisé par **Loris Perret** et **Rami Khedair**.

<div id='sketch' />

## 🖌️ Sketch
Voici les sketchs de début que nous avons prévus de faire

<br/><div align="center">
    <img  height="450" src="Images/sketch.png"/> 
</div><br/>

<div id='architecture' />

## 🏗️ Architecture à respecter
- `Views`: pour les vues
- `Components`: pour les composants
- `Modifiers`: pour les modifiers
- `Extensions`: pour les extensions
- `Resources`: pour les ressources (images, couleurs, constantes, ...)

<div id='introduction' />

## 🏢 Architecture du projet
Tout le projet est dans un workspace. Il y a ensuite deux packages : `Model (bibliothèque fournie par M.Chevaldonne)`, `DouShouQi (UI de l'application)`

<div id='introduction' />

## 📝 Message de commit
✨ `:sparkles:` -> Nouvelle fonctionnalité.  
🐛 `:bug:` -> Résolution d'un bug.  
💄 `:lipstick:` -> Travail sur le design de l'application.  
📝 `:memo:` -> Ajout de documentation.  
🍱 `:bento:` -> Ajout de ressources.  
💡 `:bulb:` -> Ajout de commentaires.  
🏗️ `:building_construction:` -> Changement dans l'architecture.

<div id='langues-disponibles' />

## 🌍 Langues Disponibles
Le projet est disponible en trois langues :
- Arabe
- Français
- Anglais

<div id='modes-de-jeu' />

## 🎮 Modes de Jeu
Le jeu propose deux modes de jeu :
- Mode Simple
- Mode Classique

<div id='technologies-utilisées' />

## 🛠️ Technologies Utilisées
Le jeu peut être joué en utilisant :
- **ARKit** : pour une expérience de réalité augmentée
- **SpriteKit** : pour une expérience de jeu en 2D classique

Ces deux technologies sont indépendantes, et il est facile d'ajouter d'autres technologies pour jouer au jeu grâce à notre architecture VM et au cycle de vie du jeu.